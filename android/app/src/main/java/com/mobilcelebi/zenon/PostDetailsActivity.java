package com.mobilcelebi.zenon;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.Result;
import com.mobilcelebi.zenon.dal.auth.Authentication;
import com.mobilcelebi.zenon.helpers.CharTranslater;
import com.mobilcelebi.zenon.helpers.Slug;
import com.mobilcelebi.zenon.models.Comment;
import com.mobilcelebi.zenon.recyclerViewAdapters.PostDetailsCommentAdapter;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PostDetailsActivity extends AppCompatActivity {

    RowItemAdapter mainAdapter;
    private RecyclerView fotoRecyclerView, postDetailsCommentRecyclerView;
    private Context context;
    private ImageView goBackButton, commentSendProfileImage, postDetailsImage,showOnMap;
    private String keyword;
    private String keywordType;
    private Requests requests;
    private TextView postDetailsDescription, postDetailsLocation, postDetailsName, postTripImagesAll, postLocationText, sendCommentButton, commentsTextView;
    private EditText sendCommentText;
    private SharedPreferences sharedPreferences;
    private Authentication authentication;
    private Types type;
    private String id;
    private PostDetailsCommentAdapter adapter;
    private ArrayList<Comment> comments;
    private ConstraintLayout commentsSendLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_details);
        init();
    }

    private void init() {
        variables();
        listeners();
        if (keywordType.equals("City")) {
            Result result = getCityInformation();
            postDetailsLocation.setVisibility(View.GONE);
            if (!result.isSuccess()) {
                Toast.makeText(this, "Bağlantıda bir sorun oluştu!", Toast.LENGTH_SHORT).show();
                finish();
                return;
            }
            type = Types.City;
            try {
                id = result.getJson().getJSONObject("data").getString("_id");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            setInfosForCity(result);
            commentsSendLayout.setVisibility(View.GONE);
            commentsTextView.setVisibility(View.GONE);
            postDetailsCommentRecyclerView.setVisibility(View.GONE);
        }
        if (keywordType.equals("Post")) {
            Result result = getPostInformation();
            if (!result.isSuccess()) {
                Toast.makeText(this, "Bağlantıda bir sorun oluştu!", Toast.LENGTH_SHORT).show();
                finish();
                return;
            }
            type = Types.Post;
            try {
                setComments(result);
                id = result.getJson().getJSONObject("data").getString("_id");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            setInfosForPost(result);
        }
        getProfilePhotoToComment();
    }

    private void getProfilePhotoToComment() {
        try {
            String id = new Authentication(sharedPreferences).getCurrentUser().getString("id");
            if (!id.isEmpty())
                Picasso.with(context)
                        .load(Config.getServerIp() + "/api/users/" + id + "/profile_photo")
                        .into(commentSendProfileImage);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setInfosForCity(Result result) {
        try {
            JSONObject data = result.getJson().getJSONObject("data");
            postDetailsDescription.setText(data.getString("description"));
            postDetailsName.setText(data.getString("name"));
            id = data.getString("_id");
            showOnMap.setOnClickListener(v->{
                Intent intent = new Intent(Intent.ACTION_VIEW);
                try {
                    intent.setData(Uri.parse("https://www.google.com/maps/search/" + data.getString("name")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                startActivity(intent);
            });
            tripImagesRecyclerView(result);
            setPhoto(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setPhoto(Result result) throws JSONException {
        if (result.getJson().getJSONObject("data").getJSONArray("images").length() > 0) {
//            Log.d("TAG", "setPhoto: " + );
            Picasso.with(context)
                    .load(result.getJson().getJSONObject("data").getJSONArray("images").get(0).toString())
                    .into(postDetailsImage);
        }
    }


    private void setInfosForPost(Result result) {
        try {
            JSONObject data = result.getJson().getJSONObject("data");
            postDetailsDescription.setText(data.getString("description"));
            postDetailsName.setText(data.getString("title"));
            postDetailsLocation.setText(data.getString("cityName"));
            tripImagesRecyclerView(result);
            showOnMap.setOnClickListener(v->{
                Intent intent = new Intent(Intent.ACTION_VIEW);
                try {
                    intent.setData(Uri.parse("https://www.google.com/maps/search/" + data.getString("title")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                startActivity(intent);
            });
            setPhoto(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setComments(Result result) throws JSONException {
        result = requests.createRequest(Config.getServerIp() + "/api/posts/" + result.getJson().getJSONObject("data").getString("_id") + "/comment", Requests.GET)
                .build();

        comments = new ArrayList<>();

        if (result.isSuccess()) {
            try {
                JSONArray data = result.getJson().getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    comments.add(new Comment(data.getJSONObject(i)));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        setCommentsRecyclerViewSettings();

        adapter = new PostDetailsCommentAdapter(context, comments);
        postDetailsCommentRecyclerView.setAdapter(adapter);
    }

    private void variables() {
        context = PostDetailsActivity.this;
        fotoRecyclerView = findViewById(R.id.fotoRecyclerView);
        showOnMap = findViewById(R.id.showOnMap);
        commentsSendLayout = findViewById(R.id.commentsSendLayout);
        commentsTextView = findViewById(R.id.commentsTextView);
        postDetailsImage = findViewById(R.id.postDetailsImage);
        sendCommentButton = findViewById(R.id.sendCommentButton);
        commentSendProfileImage = findViewById(R.id.commentSendProfileImage);
        postDetailsName = findViewById(R.id.postDetailsName);
        postDetailsLocation = findViewById(R.id.postDetailsLocation);
        sendCommentText = findViewById(R.id.sendCommentText);
        postDetailsDescription = findViewById(R.id.postDetailsDescription);
        authentication = new Authentication(sharedPreferences);
        postDetailsCommentRecyclerView = findViewById(R.id.postDetailsCommentRecyclerView);
        sharedPreferences = getSharedPreferences("mobilcelebi", MODE_PRIVATE);
//        postTripImagesAll = findViewById(R.id.postTripImagesAll);
        goBackButton = findViewById(R.id.goBackButton);
        keyword = getIntent().getExtras().getString("keyword");
        keywordType = getIntent().getExtras().getString("keywordType");
        requests = new Requests();
        authentication = new Authentication(sharedPreferences);
    }

    private void listeners() {
        goBackButton.setOnClickListener(v -> {
            finish();
        });
        sendCommentButton.setOnClickListener(v -> {
            if (authentication.getAccessToken().isEmpty()) {
                Toast.makeText(this, "Yorum gönderebilmek için giriş yapmalısın", Toast.LENGTH_SHORT).show();
                return;
            }
            if (sendCommentText.getText().toString().isEmpty()) {
                Toast.makeText(this, "Yorum boş olamaz", Toast.LENGTH_SHORT).show();
                return;
            }
            String url;
            if (type == Types.City)
                url = Config.getServerIp() + "/api/cities/" + id + "/comment";
            else
                url = Config.getServerIp() + "/api/posts/" + id + "/comment";
            requests.createRequest(url, Requests.POST)
                    .addHeader("Authorization", "Bearer: " + authentication.getAccessToken())
                    .addBodyItem("content", sendCommentText.getText().toString())
                    .addBodyItem("rating", "5")
                    .build()
                    .onSuccess(result -> {
                        if (result.isSuccess()) {
                            try {
                                comments.add(0, new Comment(result.getJson().getJSONObject("data")));
                                sendCommentText.setText("");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            adapter = new PostDetailsCommentAdapter(context, comments);
                            postDetailsCommentRecyclerView.setAdapter(adapter);
                        }
                    });
        });
    }

    private Result getCityInformation() {
        return requests.createRequest(Config.getServerIp() + "/api/city/" + Slug.toSlug(CharTranslater.translateChars(keyword)), Requests.GET)
                .build();
    }

    private Result getPostInformation() {
        return requests.createRequest(Config.getServerIp() + "/api/posts/" + Slug.toSlug(CharTranslater.translateChars(keyword)), Requests.GET)
                .build();
    }

    private void tripImagesRecyclerView(Result result) {
        ArrayList<RowImageRecyclerView> rowImageRecyclerViews = new ArrayList<>();

        try {
            JSONArray data = result.getJson().getJSONObject("data").getJSONArray("tripImages");

            for (int i = 0; i < data.length(); i++) {
                rowImageRecyclerViews.add(new RowImageRecyclerView(data.getString(i), result.getJson().getJSONObject("data").getString("_id")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setTripImagesRecyclerViewSettings();

        mainAdapter = new RowItemAdapter(context, rowImageRecyclerViews);
        fotoRecyclerView.setAdapter(mainAdapter);
    }

    private void setTripImagesRecyclerViewSettings() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        fotoRecyclerView.setLayoutManager(layoutManager);
    }

    private void setCommentsRecyclerViewSettings() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        postDetailsCommentRecyclerView.setLayoutManager(layoutManager);
    }

    enum Types {
        City,
        Post
    }
}
