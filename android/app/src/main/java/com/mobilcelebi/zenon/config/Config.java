package com.mobilcelebi.zenon.config;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.fragment.app.Fragment;

import com.mobilcelebi.zenon.SplashScreen;
import com.mobilcelebi.zenon.mainFragments.SearchFragment;

public class Config {

    private static final Context applicationContext = SplashScreen.getContextOfApplication();

    private static String DEFAULT_SERVER_IP = "http://178.157.15.179";
    public static String DEFAULT_WEB_IP = "https://mobilcelebi.com";
    private static String SERVER_SECURE = "http";
    public static String DEFAULT_PROFILE_PHOTO_STRING = "default.jpg";
    public static int APP_VERSION = -1; // -1 Development - 0 Demo - 1 Product
    public static Fragment DEFAULT_FRAGMENT = new SearchFragment();

    public static String getServerIp() {
        String serverIp;
        if(applicationContext != null){
            SharedPreferences sharedPreferences = applicationContext.getSharedPreferences("mobilcelebi",Context.MODE_PRIVATE);
            serverIp = sharedPreferences.getString("server_ip",DEFAULT_SERVER_IP);
            if(serverIp.isEmpty()){
                serverIp = DEFAULT_SERVER_IP;
            }
        }
        else{
            serverIp = DEFAULT_SERVER_IP;
        }

        if(serverIp.split("://").length <= 1)
            serverIp = getDefaultServerIp();

        return serverIp;
    }

    public static void setServerIp(String serverIp) {
        SharedPreferences sharedPreferences = applicationContext.getSharedPreferences("mobilcelebi",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("server_ip", getServerSecure() + "://" + serverIp);
        editor.apply();
    }

    public static String getDefaultProfilePhotoString() {
        return DEFAULT_PROFILE_PHOTO_STRING;
    }

    public static void setDefaultProfilePhotoString(String defaultProfilePhotoString) {
        DEFAULT_PROFILE_PHOTO_STRING = defaultProfilePhotoString;
    }

    public static String getDefaultServerIp() {
        return DEFAULT_SERVER_IP;
    }

    public static void setDefaultServerIp(String defaultServerIp) {
        DEFAULT_SERVER_IP = defaultServerIp;
    }

    public static String getServerSecure() {
        return SERVER_SECURE;
    }

    public static void setServerSecure(String serverSecure) {
        SERVER_SECURE = serverSecure;
    }

    public static int getAppVersion() {
        return APP_VERSION;
    }

    public static void setAppVersion(int appVersion) {
        APP_VERSION = appVersion;
    }

    public static Fragment getDefaultFragment() {
        return DEFAULT_FRAGMENT;
    }
}
