package com.mobilcelebi.zenon;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilcelebi.zenon.categoriesFragment.FragmentAdapter;
import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.Result;
import com.mobilcelebi.zenon.models.SearchResult;
import com.mobilcelebi.zenon.recyclerViewAdapters.SearchResultAdapter;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class SearchResultActivity extends AppCompatActivity {

    private String searchKeyword;
    private final Requests requests = new Requests();
    private RecyclerView recyclerView;
    private ViewPager viewPager;
    private TextView categoriesText;
    private FragmentAdapter fragmentAdapter;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        init();
    }

    private void init(){
        variables();
//        tabLayoutAdd();
        if(!searchKeyword.isEmpty()){
            Result result = getSearchResult();
            if(result.isSuccess()){
                recyclerViewSettings();
            }
            else{
                Toast.makeText(this, "Bağlantınızda sorun var!", Toast.LENGTH_SHORT).show();
            }
        }
        listeners();

    }

    private void listeners() {

        categoriesText.setOnClickListener(v -> {
            finish();
        });

    }

    private void variables() {
        Intent intent = getIntent();
        searchKeyword = intent.getStringExtra("searchKeyword");
        recyclerView = findViewById(R.id.searchResultRecyclerView);
        tabLayout = findViewById(R.id.tabLayout);
        viewPager= findViewById(R.id.viewPager);
        fragmentAdapter = new FragmentAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(fragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);
        categoriesText = findViewById(R.id.categoriesText);

    }

    private Result getSearchResult(){
        return requests.createRequest(Config.getServerIp() + "/api/search", Requests.GET)
                .addParam("keywords", searchKeyword)
                .build();
    }

    private void recyclerViewSettings() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(SearchResultActivity.this));
        recyclerView.addItemDecoration(new DividerItemDecoration(SearchResultActivity.this, LinearLayoutManager.VERTICAL));
    }
}