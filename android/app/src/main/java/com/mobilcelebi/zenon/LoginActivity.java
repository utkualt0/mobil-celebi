package com.mobilcelebi.zenon;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.auth.Authentication;
import com.mobilcelebi.zenon.network.NetworkManager;

public class LoginActivity extends AppCompatActivity {

    private EditText passwordEditText, mailEditText;
    private Button loginButton, quickLogin;
    private TextView alertBox, goToRegisterButton, forgetMyPassword;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String quickAccountMail, quickAccountPass, quickAccount;
    private Requests requests = new Requests();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        init();
    }

    private void init() {
        variables();
        listeners();
        netControl();
        checkQuickLogin();
    }

    private void variables() {
        mailEditText = findViewById(R.id.mailLoginEditT);
        passwordEditText = findViewById(R.id.passwordLoginEditT);
        loginButton = findViewById(R.id.loginButton);
        alertBox = findViewById(R.id.alertBoxLogin);
        goToRegisterButton = findViewById(R.id.goToRegisterButton);
        quickLogin = findViewById(R.id.quickLogin);
        forgetMyPassword = findViewById(R.id.forgetMyPassword);
        sharedPreferences = getSharedPreferences("mobilcelebi", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        quickAccountMail = sharedPreferences.getString("quickAccountMail", null);
        quickAccount = sharedPreferences.getString("quickAccount", null);
        quickAccountPass = sharedPreferences.getString("quickAccountPass", null);
    }

    private void listeners() {

        loginButton.setOnClickListener(v -> {
            requests.createRequest(Config.getServerIp() + "/api/auth/login", Requests.POST)
                    .addBodyItem("email", mailEditText.getText().toString())
                    .addBodyItem("password", passwordEditText.getText().toString())
                    .build()
                    .onSuccess(result -> {
                        if (result.isSuccess()) {
                            new Authentication(sharedPreferences).setCurrentUser(result.getJson());
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();
                        }
                    });
        });

        goToRegisterButton.setOnClickListener(v -> goToRegister());

        forgetMyPassword.setOnClickListener(v -> startActivity(new Intent(LoginActivity.this, ForgetPasswordActivity.class)));
    }

    private void checkQuickLogin() {
        if (quickAccount != null) {
            quickLogin.setVisibility(View.VISIBLE);
            quickLogin.setText(quickLogin.getText().toString() + " " + quickAccount);
        }
    }

    private void goToRegister() {
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
    }

    private void netControl() {
        if (NetworkManager.isHasConnection(this)) {
            //TODO
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }
}