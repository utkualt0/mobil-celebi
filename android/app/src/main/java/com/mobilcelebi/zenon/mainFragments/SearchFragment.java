package com.mobilcelebi.zenon.mainFragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mobilcelebi.zenon.R;
import com.mobilcelebi.zenon.RowImageTopFive;
import com.mobilcelebi.zenon.RowItemAdapterMainTopFives;
import com.mobilcelebi.zenon.SearchResultActivity;
import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.Result;
import com.mobilcelebi.zenon.dal.auth.Authentication;
import com.mobilcelebi.zenon.models.SearchResult;
import com.mobilcelebi.zenon.network.NetworkManager;
import com.mobilcelebi.zenon.recyclerViewAdapters.SearchAdapter;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchFragment extends Fragment {

    private final static int app_version = Config.getAppVersion();

    private EditText searchBox;
    private FrameLayout touchInterceptor;
    private RelativeLayout mainLayout;
    private ImageView go_up_arrow, go_down_arrow, dailyCityImage;
    private ConstraintLayout topFivePlaceLayout, topFiveMostLikedLayout, topFiveMostRaitingLayout;
    private ProgressBar loadingGifImage; //TODO use this
    private SharedPreferences sharedPreferences;
    private RecyclerView recyclerView;
    private View view;
    private Context context;
    private RelativeLayout goDiscoveryLayout;
    private final Requests requests = new Requests();
    private Authentication authentication;
    private RecyclerView topFivePlace, topFivesMostLikedRecyclerView, topFivesMostRatingRecyclerView;
    private TextView dailyCityText;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View createView = inflater.inflate(R.layout.fragment_mains_search, container, false);
        this.view = createView;
        init();
        return createView;
    }

    private void init() {
        variables();
        netControl();
        versionControl();
        intentControl();
        listeners();
        if (!authentication.getAccessToken().isEmpty()) {
            Result userInformation = getUserInformation();
            if (!userInformation.isSuccess()) {
                Result result = reLogin();
                if (result.isSuccess()) {
                    loginSuccessTasks(result);
                }
            }
        } else {
            authentication.setUserIsNull();
        }
        recyclerViewSettings();
        goUpArrow();
        goDownArrow();
        getTopFives();
        getTopFivesMostLiked();
        getTopFivesMostRating();
        setDailyCity();
    }

    private void setDailyCity() {
        Result result = getDailyCity();
        if (result.isSuccess()) {
            try {
                JSONObject data = result.getJson().getJSONObject("data");
                dailyCityText.setText(data.getString("name"));
                if (data.getJSONArray("images").length() > 0) {
                    Picasso.with(context)
                            .load(data.getJSONArray("images").get(0).toString())
                            .into(dailyCityImage);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private Result getDailyCity() {
        return requests.createRequest(Config.getServerIp() + "/api/city/day_city", Requests.GET)
                .build();
    }

    private void getTopFives() {
        Result result = requests.createRequest(Config.getServerIp() + "/api/posts?sortBy=most-commented&limit=5&page=1", Requests.GET)
                .build();
        if (result.isSuccess()) {
            top5PlaceRecyclerView(result);
        }
    }

    private void getTopFivesMostLiked() {
        Result result = requests.createRequest(Config.getServerIp() + "/api/posts?sortBy=most-liked&limit=5&page=1", Requests.GET)
                .build();
        if (result.isSuccess()) {
            topFivesMostLiked(result);
        }
    }

    private void getTopFivesMostRating() {
        Result result = requests.createRequest(Config.getServerIp() + "/api/posts?sortBy=most-liked&limit=5&page=1", Requests.GET)
                .build();
        if (result.isSuccess()) {
            topFivesMostRaiting(result);
        }
    }

    private void loginSuccessTasks(Result result) {
        authentication.setCurrentUser(result.getJson());
    }

    private Result reLogin() {
        Result reLoginResult = authentication.reLogin();

        if (!reLoginResult.isSuccess()) {
            authentication.setUserIsNull();
        }

        return reLoginResult;
    }

    private void variables() {
        sharedPreferences = context.getSharedPreferences("mobilcelebi", Context.MODE_PRIVATE);
        mainLayout = view.findViewById(R.id.mainLayout); //TODO not working
        loadingGifImage = view.findViewById(R.id.loadingProgressBar);
        recyclerView = view.findViewById(R.id.recyclerView);
        topFivesMostLikedRecyclerView = view.findViewById(R.id.topFivesMostLiked);
        topFivesMostRatingRecyclerView = view.findViewById(R.id.topFivesMostRaitingRecyclerView);
        searchBox = view.findViewById(R.id.searchBox);
        topFivePlace = view.findViewById(R.id.topFivePlace);
        topFiveMostLikedLayout = view.findViewById(R.id.topFivesMostLikedLayout);
        dailyCityText = view.findViewById(R.id.dailyCityText);
        dailyCityImage = view.findViewById(R.id.dailyCityImage);
        topFiveMostRaitingLayout = view.findViewById(R.id.topFivesMostRaitingLayout);
        topFivePlaceLayout = view.findViewById(R.id.topFivePlaceLayout);
//        goDiscoveryLayout = view.findViewById(R.id.goDiscoveryLayout);
//        miniDiscoveryLayout = view.findViewById(R.id.miniDiscoveryLayout);
        touchInterceptor = (FrameLayout) view.findViewById(R.id.searchFragment);
        go_up_arrow = view.findViewById(R.id.go_up_arrow);
        go_down_arrow = view.findViewById(R.id.go_down_arrow);
        authentication = new Authentication(sharedPreferences);
    }

    private void listeners() {
        touchInterceptor.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                if (searchBox.isFocused()) {
                    Rect outRect = new Rect();

                    searchBox.getGlobalVisibleRect(outRect);
                    if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                        searchBox.clearFocus();
                        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
            }
            return false;
        });

        searchBox.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    recyclerView.setVisibility(View.INVISIBLE);
                    go_up_arrow.setVisibility(View.INVISIBLE);
                    go_down_arrow.setVisibility(View.INVISIBLE);
                    topFiveMostLikedLayout.setVisibility(View.VISIBLE);
                    topFivePlaceLayout.setVisibility(View.VISIBLE);
                    topFiveMostRaitingLayout.setVisibility(View.VISIBLE);
                    Drawable drawable = context.getDrawable(R.drawable.searchboxbackgroundfirst);
                    searchBox.setBackground(drawable);
                    return;
                }
                recyclerView.setVisibility(View.VISIBLE);
                go_up_arrow.setVisibility(View.VISIBLE);
                topFiveMostLikedLayout.setVisibility(View.INVISIBLE);
                topFivePlaceLayout.setVisibility(View.INVISIBLE);
                topFiveMostRaitingLayout.setVisibility(View.INVISIBLE);
                Drawable drawable = context.getDrawable(R.drawable.searchboxbackgroundsecond);
                searchBox.setBackground(drawable);

                try {
                    requests.createRequest(Config.getServerIp() + "/api/search", Requests.GET)
                            .addParam("keywords", s.toString().trim())
                            .addParam("limit", "5")
                            .build()
                            .onSuccess(result -> {
                                if (result.isSuccess())
                                    setSearchDropDown(result, s.toString().trim());
                                else setSearchDropDown(result, "Bağlantı sorunu!");
                            });
                } catch (Exception e) {
                    setSearchDropDown(null, "Bağlantı sorunu!");
                    recyclerView.setClickable(false);
                }
            }
        });

    }

    private void goUpArrow() {
        go_up_arrow.setOnClickListener(v -> {
            recyclerView.setVisibility(View.INVISIBLE);
            topFiveMostLikedLayout.setVisibility(View.VISIBLE);
            topFivePlaceLayout.setVisibility(View.VISIBLE);
            topFiveMostRaitingLayout.setVisibility(View.VISIBLE);
            go_up_arrow.setVisibility(View.INVISIBLE);
            go_down_arrow.setVisibility(View.VISIBLE);
            Drawable drawable = context.getDrawable(R.drawable.searchboxbackgroundthird);
            searchBox.setBackground(drawable);
        });

    }

    private void top5PlaceRecyclerView(Result result) {
        ArrayList<RowImageTopFive> rowImageRecyclerViews = new ArrayList<>();

        try {
            JSONArray data = result.getJson().getJSONArray("data");
            for (int i = 0; i < data.length(); i++) {
                JSONArray images = data.getJSONObject(i).getJSONArray("images");
                rowImageRecyclerViews.add(new RowImageTopFive(images.length() > 0 ? images.getString(0) : "https://sinemahanedani.com/wp-content/uploads/2021/02/Hogwarts-Tarihi-Hakkinda-Bilinmesi-Gerekenler-Sinema-Hanedani-2.jpg"
                        , data.getJSONObject(i).getString("title")
                        , data.getJSONObject(i).getString("description")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setRecyclerViewSettings();

        RowItemAdapterMainTopFives mainAdapter = new RowItemAdapterMainTopFives(context, rowImageRecyclerViews);
        topFivePlace.setAdapter(mainAdapter);
    }

    private void topFivesMostLiked(Result result) {
        ArrayList<RowImageTopFive> rowImageRecyclerViews = new ArrayList<>();

        try {
            JSONArray data = result.getJson().getJSONArray("data");
            for (int i = 0; i < data.length(); i++) {
                rowImageRecyclerViews.add(new RowImageTopFive(data.getJSONObject(i).getJSONArray("images").length() > 0 ? data.getJSONObject(i).getJSONArray("images").getString(0) : "https://sinemahanedani.com/wp-content/uploads/2021/02/Hogwarts-Tarihi-Hakkinda-Bilinmesi-Gerekenler-Sinema-Hanedani-2.jpg"
                        , data.getJSONObject(i).getString("title")
                        , data.getJSONObject(i).getString("description")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setTopFivesMostLikedRecyclerView();
        RowItemAdapterMainTopFives mainAdapter = new RowItemAdapterMainTopFives(context, rowImageRecyclerViews);
        topFivesMostLikedRecyclerView.setAdapter(mainAdapter);
    }

    private void topFivesMostRaiting(Result result) {
        ArrayList<RowImageTopFive> rowImageRecyclerViews = new ArrayList<>();
        try {
            JSONArray data = result.getJson().getJSONArray("data");
            for (int i = 0; i < data.length(); i++) {
                rowImageRecyclerViews.add(new RowImageTopFive(data.getJSONObject(i).getJSONArray("images").length() > 0 ? data.getJSONObject(i).getJSONArray("images").getString(0) : "https://sinemahanedani.com/wp-content/uploads/2021/02/Hogwarts-Tarihi-Hakkinda-Bilinmesi-Gerekenler-Sinema-Hanedani-2.jpg"
                        , data.getJSONObject(i).getString("title")
                        , data.getJSONObject(i).getString("description")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setTopFivesMostRaitingRecyclerView();
        RowItemAdapterMainTopFives mainAdapter = new RowItemAdapterMainTopFives(context, rowImageRecyclerViews);
        topFivesMostRatingRecyclerView.setAdapter(mainAdapter);
    }

    private void setRecyclerViewSettings() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        topFivePlace.setLayoutManager(layoutManager);
    }

    private void setTopFivesMostLikedRecyclerView() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        topFivesMostLikedRecyclerView.setLayoutManager(layoutManager);
    }

    private void setTopFivesMostRaitingRecyclerView() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        topFivesMostRatingRecyclerView.setLayoutManager(layoutManager);
    }

    private void goDownArrow() {
        go_down_arrow.setOnClickListener(v -> {
            recyclerView.setVisibility(View.VISIBLE);
            topFiveMostLikedLayout.setVisibility(View.INVISIBLE);
            topFivePlaceLayout.setVisibility(View.INVISIBLE);
            topFiveMostRaitingLayout.setVisibility(View.INVISIBLE);
            go_down_arrow.setVisibility(View.INVISIBLE);
            go_up_arrow.setVisibility(View.VISIBLE);
            Drawable drawable = context.getDrawable(R.drawable.searchboxbackgroundsecond);
            searchBox.setBackground(drawable);
        });

    }

    private void searchCity() {
        String searchKeyword = searchBox.getText().toString().toLowerCase();
        if (TextUtils.isEmpty(searchKeyword)) {
            Toast.makeText(context, getActivity().getResources().getString(R.string.lutfen_bir_kelime_giriniz), Toast.LENGTH_SHORT).show();
        } else {
            requests.createRequest(Config.getServerIp() + "/api/explore", Requests.POST)
                    .addBodyItem("interest", searchKeyword)
                    .addHeader("access_token", authentication.getAccessToken())
                    .build();
            startActivity(new Intent(context, SearchResultActivity.class).putExtra("searchKeyword", searchBox.getText().toString()));
        }
    }

    private Result getUserInformation() {
        return requests.createRequest(Config.getServerIp() + "/api/users/getMyProfile", Requests.GET)
                .addHeader("Authorization", "Bearer: " + sharedPreferences.getString("access_token", "error"))
                .build();
    }

    private void setSearchDropDown(Result result, String searchBoxText) {
        ArrayList<SearchResult> itemList = new ArrayList<>();
        try {
            itemList.add(new SearchResult(SearchResult.POST, new JSONObject().put("title", searchBoxText)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (result != null && result.isSuccess()) {
            try {
                JSONArray citiesArray = result.getJson().getJSONArray("cities");
                for (int i = 0; i < citiesArray.length(); i++) {
                    itemList.add(new SearchResult(SearchResult.CITY, citiesArray.getJSONObject(i)));
                }
                JSONArray postsArray = result.getJson().getJSONArray("posts");
                for (int i = 0; i < postsArray.length(); i++) {
                    itemList.add(new SearchResult(SearchResult.POST, postsArray.getJSONObject(i)));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        SearchAdapter searchAdapter = new SearchAdapter(context, itemList);
        recyclerView.setAdapter(searchAdapter);
    }

    public void alertDialogOK(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Tamam", null)
                .show();
    }


    private void closeKeyboard() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            //Klavye Kapat
            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void versionControl() {
        if (app_version == 0) {
            String title = "Deneme Sürümü !";
            String message = "Şuanda uygulamanın sadece test için olan sürümünü kullanmaktasınız lütfen karşılaştığınız hataları/görsel sorunları/önerileri geri bildirim formunu doldurup bize gönderiniz!";
            alertDialogOK(title, message); //TODO gonna change
        }
    }

    private void intentControl() {
        Intent intent = getActivity().getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            searchBox.setText(intent.getExtras().getString("cityNameFromHistory"));
            if (searchBox.getText().toString().length() > 1) {
                Toast.makeText(context, "Şehir aratılıyor: " + searchBox.getText().toString(), Toast.LENGTH_SHORT).show();
                searchCity();
            }
        }
    }

    private void netControl() {
        if (NetworkManager.isHasConnection(getActivity())) {
            //TODO
        }
    }

    private void recyclerViewSettings() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
    }
}