package com.mobilcelebi.zenon.dal;

import com.squareup.okhttp.Headers;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Requests {

    private final OkHttpClient client = new OkHttpClient();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static final int GET = 1;
    public static final int POST = 2;
    public static final int DELETE = 3;
    public static final int UPDATE = 4;

    public Result get(String url, List<QueryParameter> params, List<ReqHeader> headers) {
        Request request = new Request.Builder().url(setUrl(url, params)).headers(setHeader(headers)).
                build();

        return sendRequest(request);
    }

    public Result post(String url, List<QueryParameter> params, List<ReqHeader> headers, List<ReqBody> bodyItems) {
        Request request = new Request.Builder().url(setUrl(url, params)).headers(setHeader(headers)).post(setBody(bodyItems)).
                build();

        return sendRequest(request);
    }

    public Result delete(String url, List<QueryParameter> params, List<ReqHeader> headers, List<ReqBody> bodyItems) {
        Request request = new Request.Builder().url(setUrl(url, params)).headers(setHeader(headers)).delete(setBody(bodyItems)).
                build();

        return sendRequest(request);
    }

    public Result update(String url, List<QueryParameter> params, List<ReqHeader> headers, List<ReqBody> bodyItems) {
        Request request = new Request.Builder().url(setUrl(url, params)).headers(setHeader(headers)).put(setBody(bodyItems)).
                build();

        return sendRequest(request);
    }

    private Result sendRequest(Request request) {
        try {
            Response response = client.newCall(request).execute();

            return new Result(
                    "Get Request Successful",
                    response.body().string(),
                    response.code()
            );
        } catch (IOException e) {
            e.printStackTrace();
            return new Result(e);
        }
    }

    public SpecRequest createRequest(String url, int type){
        List<QueryParameter> params = new ArrayList<>();
        List<ReqHeader> headers = new ArrayList<>();
        List<ReqBody> bodyItems = new ArrayList<>();
        if(type == GET) return new SpecRequest(url,params,headers,bodyItems,GET);
        else if(type == POST) return new SpecRequest(url,params,headers,bodyItems,POST);
        else if(type == DELETE) return new SpecRequest(url,params,headers,bodyItems,DELETE);
        else if(type == UPDATE) return new SpecRequest(url,params,headers,bodyItems,UPDATE);
        else return new SpecRequest(url,params,headers,bodyItems,GET);
    }

    private Headers setHeader(List<ReqHeader> headers) {
        Map<String, String> headerMap = new HashMap<>();

        for (ReqHeader header : headers) {
            headerMap.put(header.getHeaderKey(), header.getHeaderValue());
        }

        return Headers.of(headerMap);
    }

    private String setUrl(String url, List<QueryParameter> params) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();


        for (QueryParameter parameter : params) {
            urlBuilder.addQueryParameter(parameter.getParamKey(), parameter.getParamValue());
        }


        return urlBuilder.build().toString();
    }

    private RequestBody setBody(List<ReqBody> bodyItems) {

        StringBuilder jsonBody = new StringBuilder();
        jsonBody.append("{");

        for(ReqBody item : bodyItems){
            jsonBody.append("\"").append(item.getBodyItemKey()).append("\"").append(":").append("\"").append(item.getBodyItemValue()).append("\"");
            if(bodyItems.indexOf(item) != bodyItems.size() -1 ){
                jsonBody.append(",");
            }
        }

        jsonBody.append("}");

        return RequestBody.create(JSON,jsonBody.toString());
    }
}
