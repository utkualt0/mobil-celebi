package com.mobilcelebi.zenon.categoriesFragment;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mobilcelebi.zenon.fragmentAdapters.CategoriesResultAdapter;
import com.mobilcelebi.zenon.RowImageRecyclerView;
import com.mobilcelebi.zenon.R;
import com.mobilcelebi.zenon.RowItemAdapterFood;
import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.Result;
import com.mobilcelebi.zenon.models.SearchResult;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class HistoricalFragment extends Fragment {

    private String searchKeyword;
    private final Requests requests = new Requests();
    private View view;
    private Context context;
    private RecyclerView recyclerView;
    ArrayList<RowImageRecyclerView> rowImageRecyclerViews;
    RowItemAdapterFood mainAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View createView = inflater.inflate(R.layout.fragment_historical, container, false);
        this.view = createView;
        init();
        return createView;
    }

    private void init() {
        variables();
        if(!searchKeyword.isEmpty()){
            Result result = getSearchResult();
            if(result.isSuccess()){
                recyclerViewSettings();
                addResultsToRecyclerView(result);
            }
            else{
                Toast.makeText(context, "Bağlantınızda sorun var!", Toast.LENGTH_SHORT).show();
            }
        }
        listeners();
    }

    private void listeners() {
    }

    private void variables(){
        searchKeyword = getActivity().getIntent().getStringExtra("searchKeyword");
        recyclerView = view.findViewById(R.id.historicalRecyclerView);

    }

    private Result getSearchResult(){
        return requests.createRequest(Config.getServerIp() + "/api/search", Requests.GET)
                .addParam("keywords", searchKeyword)
                .build();
    }

    private void addResultsToRecyclerView(Result result) {
        ArrayList<SearchResult> itemList = new ArrayList<>();
        try {
            JSONArray postsArray = result.getJson().getJSONArray("posts");
            for (int i = 0; i < postsArray.length(); i++) {
                if (postsArray.getJSONObject(i).getString("category").equals("history")) {
                    itemList.add(new SearchResult(SearchResult.POST,postsArray.getJSONObject(i)));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CategoriesResultAdapter categoriesResultAdapter = new CategoriesResultAdapter(context, itemList);
        recyclerView.setAdapter(categoriesResultAdapter);
    }

    private void recyclerViewSettings() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
    }
}
