package com.mobilcelebi.zenon.dal.listeners;

import com.mobilcelebi.zenon.dal.Result;

public interface onCompleteListener {
    void completeListener(Result result);
}
