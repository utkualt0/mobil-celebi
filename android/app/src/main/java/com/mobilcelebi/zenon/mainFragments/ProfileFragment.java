package com.mobilcelebi.zenon.mainFragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilcelebi.zenon.CreateTourPopUp;
import com.mobilcelebi.zenon.FeedbackActivity;
import com.mobilcelebi.zenon.R;
import com.mobilcelebi.zenon.RegisterActivity;
import com.mobilcelebi.zenon.SeyehatnameActivity;
import com.mobilcelebi.zenon.mainFragments.profileFragment.settingsFragment.SettingsFragment;
import com.mobilcelebi.zenon.mainFragments.profileFragment.settingsFragment.ProfileSettingsFragment;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.Result;
import com.mobilcelebi.zenon.dal.auth.Authentication;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfileFragment extends Fragment {

    private View profileSettingsLayout;
    private View profileSettingsBottomSheet;
    private View view;
    private Context context;
    private SharedPreferences sharedPreferences;
    private Activity activity;
    private BottomSheetDialog bottomSheetDialog;
    private ConstraintLayout profileTourLayout, seyehatnameLayout;
    private View bottomSheetView;
    private SharedPreferences.Editor editor;
    private Authentication authentication;
    private Requests requests;
    private ImageView profilePhoto;

    private TextView usernameAccountTextView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View createView = inflater.inflate(R.layout.fragment_mains_profile, container, false);
        this.activity = getActivity();
        this.view = createView;
        init();
        return createView;
    }

    private void init() {
        variables();
        listeners();

        if (sharedPreferences.getString("currentUserInformation", "").isEmpty()) {
            startActivity(new Intent(activity, RegisterActivity.class));
            activity.finish();
            return;
        }

        Result userInformation = getUserInformation();

        if (userInformation.isSuccess()) {
            addInfosToUI(userInformation);
            getProfilePhoto();
            settingsBottomSheet();
            goToCreateTour();
            profileSettings(bottomSheetView);
            appSettings(bottomSheetView);
            feedbackLayout(bottomSheetView);
        }
        else if(userInformation.getStatus() == 401){
            if(!sharedPreferences.getString("access_token","").isEmpty()){
                Result reloginRes = authentication.reLogin();
                if(!reloginRes.isSuccess()){
                    Toast.makeText(context, "Bir sorun oluştu lütfen daha sonra tekrar dene", Toast.LENGTH_SHORT).show();
                    authentication.setUserIsNull();
                }
                else{
                    Toast.makeText(context, "Bir sorun oluştu lütfen tekrar dene", Toast.LENGTH_SHORT).show();
                }
            }
            startActivity(new Intent(activity, RegisterActivity.class));
            activity.finish();
        }
        else {
            Toast.makeText(context, "Bir sorun oluştu lütfen tekrar dene", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(activity, RegisterActivity.class));
            activity.finish();
        }

        seyehatnameLayout.setOnClickListener(v -> {
            Intent intent = new Intent(context, SeyehatnameActivity.class);
            context.startActivity(intent);
        });
    }

    private void variables() {
        profileSettingsLayout = view.findViewById(R.id.settingsMenu);
        profileTourLayout = view.findViewById(R.id.profilTourLayout);
        seyehatnameLayout = view.findViewById(R.id.profileSeyehatnameLayout);
        profilePhoto = view.findViewById(R.id.profilePhoto);
        bottomSheetDialog = new BottomSheetDialog(context, R.style.BottomSheetDialogTheme);
        bottomSheetView = LayoutInflater.from(activity.getApplicationContext())
                .inflate(
                        R.layout.fragment_settings_bottom_sheet,
                        (RelativeLayout) view.findViewById(R.id.settingsBottomSheet)
                );
        sharedPreferences = context.getSharedPreferences("mobilcelebi", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        authentication = new Authentication(sharedPreferences);
        requests = new Requests();

        usernameAccountTextView = view.findViewById(R.id.profileNameEdit);
    }

    private void settingsBottomSheet() {
        profileSettingsLayout.setOnClickListener(v -> {
            bottomSheetDialog.setContentView(bottomSheetView);
            bottomSheetDialog.show();
        });
    }

    private void goToCreateTour(){
        profileTourLayout.setOnClickListener(v -> {
            Intent intent = new Intent(context,CreateTourPopUp.class);
            startActivity(intent);
//            final Dialog dialog = new Dialog(context);
//            dialog.setContentView(R.layout.activity_create_tour);
//            Button createTourButton = (Button) dialog.findViewById(R.id.createTourButton);
//            createTourButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    dialog.dismiss();
//                }
//            });
//            dialog.show();
        });
    }

    private void profileSettings(View bottomSheetView) {
        View profileSettingsBottomSheetView = bottomSheetView.findViewById(R.id.profileSettingsBottomSheet);
        profileSettingsBottomSheetView.setOnClickListener(view -> {
            ProfileSettingsFragment nextFrag = new ProfileSettingsFragment();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, nextFrag, "findThisFragment")
                    .addToBackStack(null)
                    .commit();
            bottomSheetDialog.dismiss();
        });
    }

    private void appSettings(View bottomSheetView) {
        View profileSettingsBottomSheetView = bottomSheetView.findViewById(R.id.appSettingsLayout);
        profileSettingsBottomSheetView.setOnClickListener(view -> {
            SettingsFragment nextFrag = new SettingsFragment();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, nextFrag, "findThisFragment")
                    .addToBackStack(null)
                    .commit();
            bottomSheetDialog.dismiss();
        });
    }

    private void feedbackLayout(View bottomSheetView) {
        View profileSettingsBottomSheetView = bottomSheetView.findViewById(R.id.feedbackLayout);
        profileSettingsBottomSheetView.setOnClickListener(view -> {
            Intent intent = new Intent(context, FeedbackActivity.class);
            context.startActivity(intent);
        });
    }

    private void getProfilePhoto() {
        try {
            String id = new Authentication(sharedPreferences).getCurrentUser().getString("id");
            Picasso.with(context)
                    .load(Config.getServerIp() + "/api/users/" + id + "/profile_photo")
                    .into(profilePhoto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void listeners() {
    }

    private void addInfosToUI(Result infos) {
        try {
            JSONObject data = infos.getJson().getJSONObject("user");
            usernameAccountTextView.setText(data.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Result getUserInformation() {
        return requests.createRequest(Config.getServerIp() + "/api/users/getMyProfile", Requests.GET)
                .addHeader("Authorization", "Bearer: " + sharedPreferences.getString("access_token", ""))
                .build();

    }
}