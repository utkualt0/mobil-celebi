package com.mobilcelebi.zenon.categoriesFragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilcelebi.zenon.PostDetailsActivity;
import com.mobilcelebi.zenon.R;
import com.mobilcelebi.zenon.RowImageRecyclerView;
import com.mobilcelebi.zenon.RowItemAdapterCategories;
import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.Result;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AllFragment extends Fragment {

    private String searchKeyword;
    private final Requests requests = new Requests();
    private View view;
    private Context context;
    private TextView cityName;
    private RecyclerView historicalRrecyclerView,foodRecyclerView, socialRecyclerView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View createView = inflater.inflate(R.layout.fragment_all, container, false);
        this.view = createView;
        init();
        return createView;
    }

    private void init() {
        variables();
        if(!searchKeyword.isEmpty()){
            Result result = getSearchResult();
            if(result.isSuccess()){
                historicalRecyclerViewSettings();
                socailRecyclerViewSettings();
                foodRecyclerViewSettings();
                addResultsToRecyclerViewHistorical(result);
                addResultsToRecyclerViewSocail(result);
                addResultsToRecyclerViewFood(result);
                addResultToRecyclerViewCityName(result);
            }
            else{
                Toast.makeText(context, "Bağlantınızda sorun var!", Toast.LENGTH_SHORT).show();
            }
        }
        listeners();

    }

    private void listeners() {

        cityName.setOnClickListener(v -> {
                Intent i = new Intent(context, PostDetailsActivity.class);
                i.putExtra("keyword", cityName.getText().toString());
                i.putExtra("keywordType", "City");
                context.startActivity(i);
            });

    }

    private void variables(){
        searchKeyword = getActivity().getIntent().getStringExtra("searchKeyword");
        historicalRrecyclerView = view.findViewById(R.id.allHistoricalRecyclerView);
        foodRecyclerView = view.findViewById(R.id.allFoodRecyclerView);
        socialRecyclerView = view.findViewById(R.id.allSocialRecyclerView);
        cityName = view.findViewById(R.id.allCategoriesCityName);

    }

    private Result getSearchResult(){
        return requests.createRequest(Config.getServerIp() + "/api/search", Requests.GET)
                .addParam("keywords", searchKeyword)
                .build();
    }

    private void addResultToRecyclerViewCityName(Result result){
        try {
            JSONArray data = result.getJson().getJSONArray("cities");
            cityName.setText(data.getJSONObject(0).getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Historical
    private void addResultsToRecyclerViewHistorical(Result result) {
        ArrayList<RowImageRecyclerView> itemList = new ArrayList<>();
        try {
            JSONArray postsArray = result.getJson().getJSONArray("posts");
            for (int i = 0; i < postsArray.length(); i++) {
                if (postsArray.getJSONObject(i).getString("category").equals("history")) {
                    itemList.add(new RowImageRecyclerView(postsArray.getJSONObject(i).getJSONArray("images").length() > 0 ? postsArray.getJSONObject(i).getJSONArray("images").getString(0) : "http://sultanahmetcamii.org/wp-content/uploads/2016/02/Architecture-3.jpg",
                            postsArray.getJSONObject(i).getString("title"),
                            postsArray.getJSONObject(i).getString("description")));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RowItemAdapterCategories searchResultAdapter = new RowItemAdapterCategories(context, itemList);
        historicalRrecyclerView.setAdapter(searchResultAdapter);
    }

    private void historicalRecyclerViewSettings() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        historicalRrecyclerView.setLayoutManager(layoutManager);
    }
    //Social
    private void addResultsToRecyclerViewSocail(Result result) {
        ArrayList<RowImageRecyclerView> itemList = new ArrayList<>();
        try {
            JSONArray postsArray = result.getJson().getJSONArray("posts");
            for (int i = 0; i < postsArray.length(); i++) {
                if (postsArray.getJSONObject(i).getString("category").equals("social")) {
                    itemList.add(new RowImageRecyclerView(postsArray.getJSONObject(i).getJSONArray("images").length() > 0 ? (String) postsArray.getJSONObject(i).getJSONArray("images").get(0) : "http://sultanahmetcamii.org/wp-content/uploads/2016/02/Architecture-3.jpg",
                            postsArray.getJSONObject(i).getString("title"),
                            postsArray.getJSONObject(i).getString("description")
                            ));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RowItemAdapterCategories searchResultAdapter = new RowItemAdapterCategories(context, itemList);
        socialRecyclerView.setAdapter(searchResultAdapter);
    }
    private void socailRecyclerViewSettings() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        socialRecyclerView.setLayoutManager(layoutManager);
    }
    //Food
    private void addResultsToRecyclerViewFood(Result result) {
        ArrayList<RowImageRecyclerView> itemList = new ArrayList<>();
        try {
            JSONArray postsArray = result.getJson().getJSONArray("posts");
            for (int i = 0; i < postsArray.length(); i++) {
                if (postsArray.getJSONObject(i).getString("category").equals("food")) {
                    itemList.add(new RowImageRecyclerView(postsArray.getJSONObject(i).getJSONArray("images").length() > 0 ? (String) postsArray.getJSONObject(i).getJSONArray("images").get(0) : "https://cdn.yemek.com/mncrop/940/625/uploads/2021/04/patlicanli-pilav-yemekcom.jpg",
                            postsArray.getJSONObject(i).getString("title"),
                            postsArray.getJSONObject(i).getString("description")
                    ));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RowItemAdapterCategories searchResultAdapter = new RowItemAdapterCategories(context, itemList);
        foodRecyclerView.setAdapter(searchResultAdapter);
    }
    private void foodRecyclerViewSettings() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        foodRecyclerView.setLayoutManager(layoutManager);
    }

}