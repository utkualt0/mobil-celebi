package com.mobilcelebi.zenon;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AnimationUtils;

import com.mobilcelebi.zenon.onboarding.OnBoarding1;
import com.mobilcelebi.zenon.onboarding.OnBoarding2;
import com.mobilcelebi.zenon.onboarding.OnBoarding3;
import com.mobilcelebi.zenon.onboarding.OnBoarding4;
import com.mobilcelebi.zenon.onboarding.OnBoarding5;

public class SplashScreen extends AppCompatActivity {

    private static final int onboardingPageCount = 5;
    public static Context contextOfApplication;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        contextOfApplication = getApplicationContext();

        sharedPreferences = getSharedPreferences("mobilcelebi",Context.MODE_PRIVATE);
        ViewPager viewPager = findViewById(R.id.liquidPager);

        viewPager.startAnimation(AnimationUtils.loadAnimation(this, R.anim.o_b_anim));

        new Handler().postDelayed(()->{
            if (isFirstLogin()){
                viewPager.setAdapter(new SlideAnim(getSupportFragmentManager()));
            }
            else {
                startActivity(new Intent(SplashScreen.this, MainActivity.class));
                finish();
            }
        },500);

    }

    private boolean isFirstLogin() {
        return sharedPreferences.getBoolean("firstLogin", true);
    }

    private class SlideAnim extends FragmentStatePagerAdapter {

        public SlideAnim(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    OnBoarding1 tab1 = new OnBoarding1(SplashScreen.this);
                    return tab1;
                case 1:
                    OnBoarding2 tab2 = new OnBoarding2(SplashScreen.this);
                    return tab2;
                case 2:
                    OnBoarding3 tab3 = new OnBoarding3(SplashScreen.this);
                    return tab3;
                case 3:
                    OnBoarding4 tab4 = new OnBoarding4(SplashScreen.this);
                    return tab4;
                case 4:
                    OnBoarding5 tab5 = new OnBoarding5(SplashScreen.this);
                    return tab5;
            }
            return null;
        }

        @Override
        public int getCount() {
            return onboardingPageCount;
        }
    }

    public static Context getContextOfApplication() {
        return contextOfApplication;
    }
}