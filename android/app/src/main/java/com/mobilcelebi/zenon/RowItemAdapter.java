package com.mobilcelebi.zenon;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RowItemAdapter extends RecyclerView.Adapter<RowItemAdapter.ViewHolder> {

    ArrayList<RowImageRecyclerView> rowImageRecyclerViews;
    Context context;

    public RowItemAdapter(Context context, ArrayList<RowImageRecyclerView> rowImageRecyclerViews) {
        this.context = context;
        this.rowImageRecyclerViews = rowImageRecyclerViews;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.with(context)
                .load(Config.getServerIp() + "/api/city/" + rowImageRecyclerViews.get(position).getCityId() + "/trip_images/" + rowImageRecyclerViews.get(position).getUrl())
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return rowImageRecyclerViews.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.rowItemImage);

        }
    }
}
