package com.mobilcelebi.zenon.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class Comment {

    private JSONArray likes;
    private String likeCount;
    private String id;
    private String content;
    private int rating;
    private JSONObject user;
    private JSONObject post;
    private String createdAt;
    private boolean isLiked;

    public Comment(JSONObject comment) throws JSONException {
        this.likes = comment.getJSONArray("likes");
        this.likeCount = comment.get("likeCount").toString();
        this.id = comment.getString("_id");
        this.content = comment.getString("content");
        this.rating = comment.getInt("rating");
        this.user = comment.getJSONObject("user");
        this.post = comment.getJSONObject("post");
        this.createdAt = comment.getString("createdAt");
    }

    public Comment(String content, int rating) {
        this.content = content;
        this.rating = rating;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public JSONArray getLikes() {
        return likes;
    }

    public void setLikes(JSONArray likes) {
        this.likes = likes;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public JSONObject getUser() {
        return user;
    }

    public void setUser(JSONObject user) {
        this.user = user;
    }

    public JSONObject getPost() {
        return post;
    }

    public void setPost(JSONObject post) {
        this.post = post;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
