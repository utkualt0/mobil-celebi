package com.mobilcelebi.zenon.dal;

import org.jetbrains.annotations.NotNull;

public class QueryParameter {
    private final String paramKey;
    private final String paramValue;

    public QueryParameter(String paramKey, String paramValue) {
        this.paramKey = paramKey;
        this.paramValue = paramValue;
    }

    public String getParamKey() {
        return paramKey;
    }

    public String getParamValue() {
        return paramValue;
    }

    @NotNull
    public String toString(){
        return "Key: " + paramKey + " Value: " + paramValue;
    }
}
