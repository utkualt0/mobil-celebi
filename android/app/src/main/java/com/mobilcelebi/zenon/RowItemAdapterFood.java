package com.mobilcelebi.zenon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RowItemAdapterFood extends RecyclerView.Adapter<RowItemAdapterFood.ViewHolder> {

    ArrayList<RowImageRecyclerView> rowImageRecyclerViews;
    Context context;

    public RowItemAdapterFood(Context context, ArrayList<RowImageRecyclerView> rowImageRecyclerViews){
        this.context= context;
        this.rowImageRecyclerViews = rowImageRecyclerViews;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.food_item_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowItemAdapterFood.ViewHolder holder, int position) {
        Picasso.with(context)
                .load(rowImageRecyclerViews.get(position).getUrl())
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return rowImageRecyclerViews.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.foodItemRow);

        }
    }
}