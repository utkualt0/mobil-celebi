package com.mobilcelebi.zenon;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.auth.Authentication;
import com.mobilcelebi.zenon.network.NetworkManager;

import org.json.JSONException;

public class RegisterActivity extends AppCompatActivity {

    private EditText mailEditText,passwordEditText,passwordVerifyEditText,usernameEditText;
    private Button registerButton;
    private TextView goToLogInButton,alertBox,beGuest;
    private CheckBox readAndAcceptRegister;
    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
    }

    private void init() {
        variables();
        listeners();
        netControl();
        setCheckBox();
    }

    private void setCheckBox() {
        String checkBoxText = "<a href='https://www.mobilcelebi.com/policies' > Kullanıcı Sözleşmesini</a> okudum ve kabul ediyorum.";

        readAndAcceptRegister.setText(Html.fromHtml(checkBoxText));
        readAndAcceptRegister.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void variables() {
        mailEditText = (EditText)findViewById(R.id.mailRegisterEditText);
        passwordEditText = (EditText)findViewById(R.id.passwordRegisterEditText);
        passwordVerifyEditText = (EditText)findViewById(R.id.passwordVerifyRegisterEditText);
        usernameEditText = (EditText)findViewById(R.id.usernameRegisterEditText);
        registerButton = (Button) findViewById(R.id.registerButton);
        goToLogInButton = (TextView) findViewById(R.id.goToLoginButton);
        beGuest = (TextView) findViewById(R.id.beGuestRegister);
        readAndAcceptRegister = findViewById(R.id.readAndAcceptRegister);
        alertBox = (TextView) findViewById(R.id.alertBoxRegister);
    }

    private void listeners() {
        goToLogInButton.setOnClickListener(v -> goToLogin());
        registerButton.setOnClickListener(v -> register());
        beGuest.setOnClickListener(v -> continueAsGuest());
    }

    private void register() {
        if(netControl()){
            if(usernameEditText.getText().toString().isEmpty() || passwordEditText.getText().toString().isEmpty() || mailEditText.getText().toString().isEmpty()){
                Toast.makeText(this, "Lütfen boş yer bırakmayınız!", Toast.LENGTH_SHORT).show();
                return;
            }
            if(!readAndAcceptRegister.isChecked()){
                Toast.makeText(this, "Kullanıcı sözleşmesini kabul etmeden kayıt olamazsınız!", Toast.LENGTH_SHORT).show();
                return;
            }
            if(!passwordEditText.getText().toString().equals(passwordVerifyEditText.getText().toString())){
                alertDialogOK(getString(R.string.hata),getString(R.string.parolalar_eslesmiyor));
                passwordVerifyEditText.setText("");
                return;
            }
            Requests requests = new Requests();
            requests.createRequest(Config.getServerIp() + "/api/auth/register",Requests.POST)
                    .addBodyItem("name",usernameEditText.getText().toString())
                    .addBodyItem("email",mailEditText.getText().toString())
                    .addBodyItem("password",passwordEditText.getText().toString())
                    .build()
                    .onSuccess(result -> {
                        try {
                            String access_token = result.getJson().get("access_token").toString();
                            SharedPreferences sharedPreferences = getSharedPreferences("mobilcelebi",Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("access_token",access_token);
                            editor.apply();
                            startActivity(new Intent(RegisterActivity.this,MainActivity.class));
                            //TODO kayıt sonrası sayfaya geçebilir örnek olarak foto ayarlama gibi...
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            alertDialogOK(getString(R.string.hata),getString(R.string.hata));
                            return;
                        }
                    });
        }
    }

    private Boolean netControl() {
        Boolean isNet = NetworkManager.isHasConnection(RegisterActivity.this);
        if(!isNet){
            String title = "İnternet bağlantısı yok";
            String message = "Cihazınızda internet bağlantısı bulunamadı uygulamayı kullanmaya devam edebilirsiniz fakat aldığınız bilgilerin bazıları güncel olmayabilir";
            alertDialogOK(title, message);
        }
        return isNet;
    }

    private void goToLogin() {
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        finish();
    }

    private void continueAsGuest() {
        new Authentication(getSharedPreferences("mobilcelebi",MODE_PRIVATE)).setUserIsNull();
        startActivity(new Intent(RegisterActivity.this,MainActivity.class));
        finish();
    }

    public void alertDialogOK(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
        builder.setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Tamam", null)
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(RegisterActivity.this,MainActivity.class));
        finish();
    }
}