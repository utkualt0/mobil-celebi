package com.mobilcelebi.zenon;

import android.content.Context;
import android.content.Intent;
import android.inputmethodservice.Keyboard;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.models.SearchResult;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

public class RowItemAdapterCategories extends RecyclerView.Adapter<RowItemAdapterCategories.ViewHolder> {

    ArrayList<RowImageRecyclerView> rowImageRecyclerViews;
    Context context;

    public RowItemAdapterCategories(Context context, ArrayList<RowImageRecyclerView> rowImageRecyclerViews) {
        this.context = context;
        this.rowImageRecyclerViews = rowImageRecyclerViews;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_all_categories, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.postPostName.setText(rowImageRecyclerViews.get(position).getPostName());
        holder.categoriesResultDescription.setText(rowImageRecyclerViews.get(position).getPostDescription());
        holder.itemView.setOnClickListener(v -> {
                Intent i = new Intent(context,PostDetailsActivity.class);
                i.putExtra("keyword",holder.postPostName.getText().toString());
                i.putExtra("keywordType","Post");
                context.startActivity(i);
            });

        Picasso.with(context)
                .load(rowImageRecyclerViews.get(position).getUrl())
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return rowImageRecyclerViews.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView categoriesResultDescription;
        TextView postPostName, postLikeCount, postCommentCount, cityName;
        ConstraintLayout cityLayout;
        ConstraintLayout postLayout;
        ImageView postShareLayout;
        ImageView socialItemRow;
        ImageView postLikeButton;
        ImageView rowItemImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.rowItemImage);
            categoriesResultDescription = itemView.findViewById(R.id.categoriesResultDescription);
            postLayout = itemView.findViewById(R.id.postLayout);
            cityName = itemView.findViewById(R.id.allCategoriesCityName);
            cityLayout = itemView.findViewById(R.id.cityLayout);
            postPostName = itemView.findViewById(R.id.allCategoriesPostName);
            postLikeCount = itemView.findViewById(R.id.likeCount);
            postCommentCount = itemView.findViewById(R.id.categoriesCommentCount);
            postShareLayout = itemView.findViewById(R.id.categoriesShareIcon);
            postLikeButton = itemView.findViewById(R.id.likeIcon);
            socialItemRow = itemView.findViewById(R.id.socialItemRow);

        }
    }

}
