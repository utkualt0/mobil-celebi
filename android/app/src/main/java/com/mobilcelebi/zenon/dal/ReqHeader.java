package com.mobilcelebi.zenon.dal;

import org.jetbrains.annotations.NotNull;

public class ReqHeader {
    private final String headerKey;
    private final String headerValue;

    public ReqHeader(String headerKey, String headerValue) {
        this.headerKey = headerKey;
        this.headerValue = headerValue;
    }

    public String getHeaderKey() {
        return headerKey;
    }

    public String getHeaderValue() {
        return headerValue;
    }

    @NotNull
    public String toString(){
        return "Key: " + headerKey + " Value: " + headerValue;
    }
}
