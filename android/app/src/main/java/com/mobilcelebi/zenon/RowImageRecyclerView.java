package com.mobilcelebi.zenon;

import org.json.JSONObject;

public class RowImageRecyclerView extends JSONObject {
    private String url;
    private String cityId;
    private String cityName;
    private String postName;
    private String postDescription;

    public RowImageRecyclerView(String url, String cityId) {
        this.url = url;
        this.cityId = cityId;
    }

    public RowImageRecyclerView(String url, String postName, String postDescription) {
        this.url = url;
        this.postName = postName;
        this.postDescription = postDescription;
    }

    public RowImageRecyclerView(String cityName){
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getPostDescription() {
        return postDescription;
    }

    public void setPostDescription(String postDescription) {
        this.postDescription = postDescription;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }
}
