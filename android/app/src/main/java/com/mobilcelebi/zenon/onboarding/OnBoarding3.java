package com.mobilcelebi.zenon.onboarding;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.mobilcelebi.zenon.MainActivity;
import com.mobilcelebi.zenon.R;

public class OnBoarding3 extends Fragment {
    private TextView atlaB3;
    private Activity splashScreen;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private ProgressBar onBoardingProgressBar3;

    public OnBoarding3(Activity splashScreen) {
        this.splashScreen = splashScreen;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_onboarding_3, container,false);
        atlaB3 = root.findViewById(R.id.skipButtonP3);
        onBoardingProgressBar3 = root.findViewById(R.id.onBoardingProgressBar3);
        sharedPreferences = splashScreen.getSharedPreferences("mobilcelebi",Context.MODE_PRIVATE);
        editor= sharedPreferences.edit();

        atlaB3.setOnClickListener(v->{
            startActivity(new Intent(splashScreen, MainActivity.class));
            onBoardingProgressBar3.setVisibility(View.VISIBLE);
            splashScreen.finish();
            editor.putBoolean("firstLogin",false);
            editor.commit();
        });
        return root;
    }
}
