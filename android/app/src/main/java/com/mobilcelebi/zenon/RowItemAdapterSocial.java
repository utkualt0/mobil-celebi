package com.mobilcelebi.zenon;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.helpers.CharTranslater;
import com.mobilcelebi.zenon.helpers.Slug;
import com.mobilcelebi.zenon.models.SearchResult;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RowItemAdapterSocial extends RecyclerView.Adapter<RowItemAdapterSocial.ViewHolder> {

    ArrayList<SearchResult> itemList;
    Context context;

    public RowItemAdapterSocial(Context context, ArrayList<SearchResult> itemList){
        this.context= context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public RowItemAdapterSocial.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.social_item_row,parent,false);
        return new RowItemAdapterSocial.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowItemAdapterSocial.ViewHolder holder, int position) {
        if(itemList.get(position).getType().equals("Post")){
            JSONObject data = itemList.get(position).getData();
            try {
                String image;
                if (data.getJSONArray("images").length()>0){
                    image = data.getJSONArray("images").get(0).toString();
                }
                else{
                    image = "https://www.tripadvisor.com/Attraction_Review-g1066456-d2311984-Reviews-Tokyo_Camii_Turkish_Culture_Center-Shibuya_Tokyo_Tokyo_Prefecture_Kanto.html";
                }
                Picasso.with(context)
                        .load(image)
                        .into(holder.imageView);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.socialItemRow);

        }
    }
}
