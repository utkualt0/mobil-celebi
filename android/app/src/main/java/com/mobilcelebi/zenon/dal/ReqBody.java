package com.mobilcelebi.zenon.dal;

import org.jetbrains.annotations.NotNull;

public class ReqBody {
    private final String bodyItemKey;
    private final String bodyItemValue;

    public ReqBody(String bodyItemKey, String bodyItemValue) {
        this.bodyItemKey = bodyItemKey;
        this.bodyItemValue = bodyItemValue;
    }

    public String getBodyItemKey() {
        return bodyItemKey;
    }

    public String getBodyItemValue() {
        return bodyItemValue;
    }

    @NotNull
    public String toString(){
        return "Key: " + bodyItemKey + " Value: " + bodyItemValue;
    }
}
