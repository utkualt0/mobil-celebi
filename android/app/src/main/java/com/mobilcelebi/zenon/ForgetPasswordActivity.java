package com.mobilcelebi.zenon;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgetPasswordActivity extends AppCompatActivity {

    private EditText mailEditText;
    private Button sendResetMailButton;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        init();
    }

    private void init() {
        variables();
        listeners();
    }

    private void variables() {
        mailEditText = (EditText)findViewById(R.id.mailForgetEditText);
        sendResetMailButton = (Button)findViewById(R.id.sendResetMailButton);
        mAuth = FirebaseAuth.getInstance();
    }

    private void listeners() {
        sendResetMailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPassword();
            }
        });
    }

    private void resetPassword() {
        String emailAddress = mailEditText.getText().toString();

        mAuth.sendPasswordResetEmail(emailAddress)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(ForgetPasswordActivity.this, "Sıfırlama Postası Gönderildi Mail Kutunuzu Kontrol Edin!", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(ForgetPasswordActivity.this,LoginActivity.class));
                        }
                        else{
                            Toast.makeText(ForgetPasswordActivity.this, "Mail Gönderilemedi Posta Adresiniz Yanlış Olabilir", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}