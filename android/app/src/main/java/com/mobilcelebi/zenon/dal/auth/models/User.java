package com.mobilcelebi.zenon.dal.auth.models;

import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class User {

    private String id;
    private String name;
    private String email;
    private String role;
    private int adminLevel;
    private String profileImage;
    private boolean verified;
    private boolean blocked;
    private JSONArray commentArray;
    private JSONArray interest;
    private JSONArray ownTours;
    private JSONArray attendedTours;
    private String createdAt;
    private JSONObject userInformation;

    public User(JSONObject userInformation) throws JSONException {
        this.userInformation = userInformation;
        this.id = userInformation.getString("_id");
        this.name = userInformation.getString("name");
        this.email = userInformation.getString("email");
        this.role = userInformation.getString("name");
        this.adminLevel = userInformation.getInt("adminLevel");
        this.profileImage = userInformation.getString("profile_image");
        this.verified = userInformation.getBoolean("verified");
        this.blocked = userInformation.getBoolean("blocked");
        this.createdAt = userInformation.getString("createdAt");
        this.commentArray = userInformation.getJSONArray("comments");
        this.interest = userInformation.getJSONArray("interests");
        this.attendedTours = userInformation.getJSONArray("attendedTours");
        this.ownTours = userInformation.getJSONArray("ownTours");
    }

    public void saveAsCurrentUser(SharedPreferences sharedPreferences) {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("currentUserInformation",toString());
        editor.apply();
    }

    public String toString(){
        return  "{" +
                "\"id\":"+"\""+ id +"\"," +
                "\"name\":"+"\""+ name +"\"," +
                "\"email\":"+"\""+ email +"\"," +
                "\"role\":"+"\""+ role +"\"," +
                "\"adminLevel\":"+"\""+ adminLevel +"\"," +
                "\"profileImage\":"+"\""+ profileImage +"\"," +
                "\"verified\":"+"\""+ verified +"\"," +
                "\"blocked\":"+"\""+ blocked +"\"," +
                "\"commentArray\":"+""+ commentArray.toString() +"," +
                "\"interest\":"+""+ interest.toString() +"," +
                "\"createdAt\":"+"\""+ createdAt +"\","+
                "\"ownTours\":"+ ownTours.toString() +","+
                "\"attendedTours\":"+ attendedTours.toString()+
                "}";
    }

}
