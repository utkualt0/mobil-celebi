package com.mobilcelebi.zenon.recyclerViewAdapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mobilcelebi.zenon.R;
import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.Result;
import com.mobilcelebi.zenon.dal.auth.Authentication;
import com.mobilcelebi.zenon.models.Comment;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import java.util.List;

public class PostDetailsCommentAdapter extends RecyclerView.Adapter<PostDetailsCommentAdapter.CommentViewHolder> {

    Context context;
    List<Comment> commentList;
    SharedPreferences sharedPreferences;
    Requests requests;

    public PostDetailsCommentAdapter(Context context, List<Comment> commentList) {
        this.context = context;
        this.commentList = commentList;
        sharedPreferences = context.getSharedPreferences("mobilcelebi", Context.MODE_PRIVATE);
        requests = new Requests();
    }

    @NonNull
    @Override
    public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_layout, parent, false);
        return new PostDetailsCommentAdapter.CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentViewHolder holder, int position) {
        holder.commentContent.setText(commentList.get(position).getContent());
        holder.commentLikeCount.setText(commentList.get(position).getLikeCount());
        holder.commentRating.setRating(commentList.get(position).getRating());
        //Is liked
        try {
            if (new Authentication(sharedPreferences).getAccessToken().isEmpty() || !commentList.toString().contains(new Authentication(sharedPreferences).getCurrentUser().getString("id"))) {
                holder.commentLikeButton.setImageResource(R.drawable.like_icon_border);
                commentList.get(position).setLiked(false);
            } else {
                holder.commentLikeButton.setImageResource(R.drawable.like_icon_fill);
                commentList.get(position).setLiked(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Profile Photo
        try {
            String id = commentList.get(position).getUser().getString("_id");
            Picasso.with(context)
                    .load(Config.getServerIp() + "/api/users/" + id + "/profile_photo")
                    .into(holder.commentProfileImage);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Like Button
        holder.commentLikeButton.setOnClickListener(v -> {
            requests.createRequest(
                    commentList.get(position).isLiked() ?
                            Config.getServerIp() + "/api/posts/" + commentList.get(position).getPost() + "/comment/" + commentList.get(position).getId() + "/undo_like" :
                            Config.getServerIp() + "/api/posts/" + commentList.get(position).getPost() + "/comment/" + commentList.get(position).getId() + "/like",
                    Requests.GET)
                    .addHeader(
                            "Authorization",
                            "Bearer: " + sharedPreferences.getString("access_token", ""))
                    .build()
                    .onSuccess(result -> {
                        if (result.isSuccess()) {
                            try {
                                if (result.getJson().getJSONObject("data").getJSONArray("likes").toString().contains(new Authentication(sharedPreferences).getCurrentUser().getString("id"))) {
                                    holder.commentLikeButton.setImageResource(R.drawable.like_icon_fill);
                                    commentList.get(position).setLiked(true);
                                } else {
                                    holder.commentLikeButton.setImageResource(R.drawable.like_icon_border);
                                    commentList.get(position).setLiked(false);
                                }
                                holder.commentLikeCount.setText(result.getJson().getJSONObject("data").get("likeCount").toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (result.getStatus() == 401) {
                            Result reLoginResult = new Authentication(sharedPreferences).reLogin();
                            if (!reLoginResult.isSuccess()) {
                                new Authentication(sharedPreferences).setUserIsNull();
                                Toast.makeText(context, "Bir sorun oluştu!", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Bir sorun oluştu lütfen tekrar dene!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, "Bir hata oluştu lütfen tekrar dene!", Toast.LENGTH_SHORT).show();
                        }
                    });
        });
        //More Button
        if (!new Authentication(sharedPreferences).getAccessToken().isEmpty())
            holder.commentMoreButton.setOnClickListener(v -> {
                PopupMenu popup = new PopupMenu(context, v);
                popup.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.commentPopUpItemDelete:
                            removeComment(position);
                            return true;
                        case R.id.commentPopUpItemReport:
                            reportUser(position);
                            return true;
                        default:
                            return false;
                    }
                });
                popup.inflate(R.menu.comment_pop_up);
                //is your message
                try {
                    if (!commentList.get(position).getUser().getString("_id").equals(new Authentication(sharedPreferences).getCurrentUser().getString("id"))) {
                        popup.getMenu().findItem(R.id.commentPopUpItemDelete).setVisible(false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                popup.show();
            });
        else
            holder.commentMoreButton.setOnClickListener(v-> Toast.makeText(context, "Bu özellikleri kullanabilmek için giriş yapmalısın!", Toast.LENGTH_SHORT).show());
    }

    void reportUser(int position) {
        try {
            requests.createRequest(Config.getServerIp() + "/api/users/" + commentList.get(position).getUser().getString("_id") + "/report", Requests.GET)
                    .addHeader("Authorization", "Bearer: " + new Authentication(sharedPreferences).getAccessToken())
                    .addBodyItem("type", "Comment Report")
                    .addBodyItem("description", "Bildirilen yorumun içeriği: " + commentList.get(position).getContent())
                    .build()
                    .onSuccess(result -> {
                        if (result.isSuccess()) {
                            Toast.makeText(context, "Kullanıcıyı bildirdin!", Toast.LENGTH_SHORT).show();
                        } else if (result.getStatus() == 401) {
                            Result reLoginResult = new Authentication(sharedPreferences).reLogin();
                            if (!reLoginResult.isSuccess()) {
                                new Authentication(sharedPreferences).setUserIsNull();
                                Toast.makeText(context, "Bir sorun oluştu!", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Bir sorun oluştu lütfen tekrar dene!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, "Bir hata oluştu", Toast.LENGTH_SHORT).show();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void removeComment(int position) {
        requests.createRequest(Config.getServerIp() + "/api/admin/comment/" + commentList.get(position).getId(), Requests.DELETE)
                .addHeader("Authorization", "Bearer: " + sharedPreferences.getString("access_token", ""))
                .build()
                .onSuccess(result -> {
                    if (result.isSuccess()) {
                        commentList.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, commentList.size());
                        Toast.makeText(context, "Silme işlemi başarılı!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Hata!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    class CommentViewHolder extends RecyclerView.ViewHolder {

        ImageView commentProfileImage, commentLikeButton, commentMoreButton;
        TextView commentContent, commentLikeCount;
        RatingBar commentRating;

        public CommentViewHolder(@NonNull View itemView) {
            super(itemView);
            commentLikeCount = itemView.findViewById(R.id.commentLikeCount);
            commentLikeButton = itemView.findViewById(R.id.commentLikeButton);
            commentContent = itemView.findViewById(R.id.commentDescription);
            commentProfileImage = itemView.findViewById(R.id.commentProfileImage);
            commentRating = itemView.findViewById(R.id.commentRating);
            commentMoreButton = itemView.findViewById(R.id.commentMoreButton);
        }
    }
}
