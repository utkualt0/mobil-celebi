package com.mobilcelebi.zenon;

public class RowImageTopFive {
    private String url;
    private String cityName;
    private String cityDescription;


    public RowImageTopFive(String url) {
        this.url = url;
    }

    public RowImageTopFive(String url, String cityName, String cityDescription) {
        this.url = url;
        this.cityName = cityName;
        this.cityDescription = cityDescription;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityDescription() {
        return cityDescription;
    }

    public void setCityDescription(String cityDescription) {
        this.cityDescription = cityDescription;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
