package com.mobilcelebi.zenon;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.Result;
import com.mobilcelebi.zenon.dal.auth.Authentication;
import com.mobilcelebi.zenon.helpers.CharTranslater;
import com.mobilcelebi.zenon.helpers.Slug;
import com.mobilcelebi.zenon.models.Comment;
import com.mobilcelebi.zenon.recyclerViewAdapters.PostDetailsCommentAdapter;
import com.mobilcelebi.zenon.recyclerViewAdapters.SeyehatnameAdapter;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class SeyehatnameActivity extends AppCompatActivity {

    private RecyclerView seyehatnameRecyclerView;
    private Requests requests = new Requests();
    private ImageView goBackArrow;
    private SharedPreferences sharedPreferences;
    private TextView seyahatnameYourComments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seyehatname);
//        init();
        variables();
        goBackArrowButton();
        setSeyahatname();
    }

    private void setSeyahatname() {
        Result result = getSeyahatname();
        if(result.isSuccess()){
            ArrayList<Comment> commentsArrayList = new ArrayList<>();
            try {
                JSONArray comments = result.getJson().getJSONObject("user").getJSONArray("comments");
                seyahatnameYourComments.setText(Integer.toString(comments.length()));
                for (int i = 0;i < comments.length();i++){
                    commentsArrayList.add(new Comment(comments.getJSONObject(0)));
                }
                setSeyahatnameRecyclerViewSettings();
                SeyehatnameAdapter adapter = new SeyehatnameAdapter(this, commentsArrayList);
                seyehatnameRecyclerView.setAdapter(adapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else{
            Toast.makeText(this, "Bir sorun oluştu lütfen bir daha deneyiniz", Toast.LENGTH_SHORT).show();
        }
    }

    private Result getSeyahatname(){
        return requests.createRequest(Config.getServerIp() + "/api/users/getMyProfile",Requests.GET)
                .addHeader("Authorization","Bearer: " + new Authentication(sharedPreferences).getAccessToken())
                .build();
    }

    private void variables(){
        goBackArrow = findViewById(R.id.goBackArrow);
        seyehatnameRecyclerView = findViewById(R.id.seyehatnameRecyclerView);
        seyahatnameYourComments = findViewById(R.id.seyahatnameYourComments);
        sharedPreferences = getSharedPreferences("mobilcelebi",MODE_PRIVATE);
    }

    private void goBackArrowButton(){
        goBackArrow.setOnClickListener(v -> {
            finish();
        });
    }

    private void setSeyahatnameRecyclerViewSettings() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        seyehatnameRecyclerView.setLayoutManager(layoutManager);
    }

}