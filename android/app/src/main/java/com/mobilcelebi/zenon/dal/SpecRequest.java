package com.mobilcelebi.zenon.dal;


import java.util.List;

public class SpecRequest {
    private List<QueryParameter> params;
    private List<ReqHeader> headers;
    private List<ReqBody> bodyItems;
    private String url;
    private final int type;
    private final Requests requests = new Requests();

    public SpecRequest(String url, List<QueryParameter> params, List<ReqHeader> headers, List<ReqBody> bodyItems, int type) {
        this.params = params;
        this.headers = headers;
        this.bodyItems = bodyItems;
        this.url = url;
        this.type = type;
    }

    public SpecRequest addHeader(String key, String value){
        headers.add(new ReqHeader(key,value));
        return this;
    }

    public SpecRequest addParam(String key, String value){
        params.add(new QueryParameter(key,value));
        return this;
    }

    public SpecRequest addBodyItem(String key, String value){
        bodyItems.add(new ReqBody(key,value));
        return this;
    }

    public Result build(){
        if(this.type == Requests.DELETE) return requests.delete(url,params,headers,bodyItems);
        if(this.type == Requests.UPDATE) return requests.update(url,params,headers,bodyItems);
        if(this.type == Requests.POST) return requests.post(url,params,headers,bodyItems);
        if(this.type == Requests.GET) return requests.get(url,params,headers);
        return new Result("Error","Build Error!",500);
    }

    public List<QueryParameter> getParams() {
        return params;
    }

    public void setParams(List<QueryParameter> params) {
        this.params = params;
    }

    public List<ReqHeader> getHeaders() {
        return headers;
    }

    public void setHeaders(List<ReqHeader> headers) {
        this.headers = headers;
    }

    public List<ReqBody> getBodyItems() {
        return bodyItems;
    }

    public void setBodyItems(List<ReqBody> bodyItems) {
        this.bodyItems = bodyItems;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
