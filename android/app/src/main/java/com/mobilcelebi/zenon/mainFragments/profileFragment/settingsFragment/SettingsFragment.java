package com.mobilcelebi.zenon.mainFragments.profileFragment.settingsFragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobilcelebi.zenon.R;
import com.mobilcelebi.zenon.RegisterActivity;
import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.mainFragments.ProfileFragment;

public class SettingsFragment extends Fragment {

    private View view;
    private Context context;
    private TextView settingsText;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Button logOutButton;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View createView = inflater.inflate(R.layout.fragment_settings, container, false);
        this.view = createView;
        init();
        return createView;
    }

    private void init() {
        variables();
        listeners();
    }

    private void variables() {
        sharedPreferences = context.getSharedPreferences("mobilcelebi", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        settingsText = view.findViewById(R.id.settingsText);
        logOutButton = view.findViewById(R.id.logOutButton);
    }

    private void listeners() {

        settingsText.setOnClickListener(v -> {
            ProfileFragment nextFrag = new ProfileFragment();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, nextFrag, "findThisFragment")
                    .addToBackStack(null)
                    .commit();
        });

        logOutButton.setOnClickListener(v -> {
            editor.putString("access_token", "");
            editor.apply();
            startActivity(new Intent(context, RegisterActivity.class));
            getActivity().finish();
        });

    }

}