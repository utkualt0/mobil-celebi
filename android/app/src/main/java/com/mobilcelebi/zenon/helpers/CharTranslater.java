package com.mobilcelebi.zenon.helpers;

public class CharTranslater {
    public static String translateChars(String word)
    {
        char[] oldValue = new char[] { 'ö', 'Ö', 'ü', 'Ü', 'ç', 'Ç', 'İ', 'ı', 'Ğ', 'ğ', 'Ş', 'ş' };
        char[] newValue = new char[] { 'o', 'O', 'u', 'U', 'c', 'C', 'I', 'i', 'G', 'g', 'S', 's' };
        for (int i = 0; i < oldValue.length; i++)
        {
            word = word.replace(oldValue[i], newValue[i]);
        }
        return word;
    }
}
