package com.mobilcelebi.zenon.models;

import org.json.JSONObject;

public class SearchResult {

    public static Types CITY = Types.City;
    public static Types POST = Types.Post;
    private Types type;
    private JSONObject data;

    public SearchResult(Types type, JSONObject data) {
        this.type = type;
        this.data = data;
    }

    private Boolean isTypesInclude(String value) {
        for (Types type : Types.values()) {
            if (type.name().equals(value)) {
                return true;
            }
        }
        return false;
    }

    public Types getType() {
        return type;
    }

    public void setType(Types type) {
        this.type = type;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public enum Types {
        City,
        Post
    }
}
