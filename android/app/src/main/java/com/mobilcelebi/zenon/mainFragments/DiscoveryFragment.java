package com.mobilcelebi.zenon.mainFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilcelebi.zenon.R;
import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.Result;
import com.mobilcelebi.zenon.dal.auth.Authentication;
import com.mobilcelebi.zenon.models.DiscoverImage;
import com.mobilcelebi.zenon.recyclerViewAdapters.discoverImageAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class DiscoveryFragment extends Fragment {

    private View view;
    private Context context;
    private ViewPager2 imageViewPager;
    private Requests requests;
    private SharedPreferences sharedPreferences;
    private Authentication authentication;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View createView = inflater.inflate(R.layout.fragment_mains_discovery, container, false);
        this.view = createView;
        init();
        return createView;
    }

    private void init() {
        variables();
        getImages();
        try {
            if(new Authentication(sharedPreferences).getCurrentUser().isNull("id")) {
                Toast.makeText(context, "Bu özelliği kullanabilmek için giriş yapmalısın!", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void variables() {
        imageViewPager = view.findViewById(R.id.discoverImageViewPager);
        requests = new Requests();
        sharedPreferences = context.getSharedPreferences("mobilcelebi", Context.MODE_PRIVATE);
        authentication = new Authentication(sharedPreferences);
    }

    private void getImages() {
        List<DiscoverImage> imageItems = new ArrayList<>();
        requests.createRequest(Config.getServerIp() + "/api/explore",Requests.GET)
                .addHeader("Authorization","Bearer: " + sharedPreferences.getString("access_token", "error"))
                .build()
                .onSuccess(result -> {
                    if(result.isSuccess()){
                        try {
                            JSONObject data = result.getJson().getJSONObject("data");
                            JSONArray cities;
                            JSONArray posts = data.getJSONArray("posts");
                            for(int i = 0;i < posts.length();i++){
                                DiscoverImage item = new DiscoverImage();
                                item.postID = posts.getJSONObject(i).getString("_id");
                                item.likeCount = posts.getJSONObject(i).get("likeCount").toString();
                                item.commentCount = posts.getJSONObject(i).get("commentCount").toString();
                                item.imageTitle = posts.getJSONObject(i).getString("title");
                                item.likeArray = posts.getJSONObject(i).getJSONArray("likes");
                                item.imageDescription = posts.getJSONObject(i).getString("description");
                                try {
                                    item.like = posts.getJSONObject(i).get("likes").toString().contains(new Authentication(sharedPreferences).getCurrentUser().get("id").toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                if(posts.getJSONObject(i).getJSONArray("images").length() > 0){
                                    item.imageURL = posts.getJSONObject(i).getJSONArray("images").getString(0);
                                }
                                else{
                                    item.imageURL = "https://i.pinimg.com/236x/28/d2/ea/28d2ea0e4ad6ac6274b0c0563c10aeea.jpg";
                                }
                                imageItems.add(item);
                            }
                            imageViewPager.setAdapter(new discoverImageAdapter(context,imageItems));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else if(result.getStatus() == 401){
                        Result reLoginResult = authentication.reLogin();

                        if (!reLoginResult.isSuccess()) {
                            authentication.setUserIsNull();
                            Toast.makeText(context, "Bu özelliği kullanabilmek için giriş yapmalısın!", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(context, "Bir sorun oluştu lütfen tekrar dene", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}