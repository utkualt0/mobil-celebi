package com.mobilcelebi.zenon.network;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;

public class NetworkManager {

    //İnternet kontrol etme
    public static boolean isHasConnection(Activity activity) {
        /*
        * Activity: Fonksiyonun çağırıldığı activity
        * */
        if (netControl(activity)) {
            return true;
        } //Eğer internet varsa hiçbir şey yapma
        else { //Eğer internet yoksa
            return false;
        }
    }

    //İnternet kontrolu için manager
    private static boolean netControl(Activity activity) {
        ConnectivityManager manager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager.getActiveNetworkInfo() != null
                && manager.getActiveNetworkInfo().isAvailable()
                && manager.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}
