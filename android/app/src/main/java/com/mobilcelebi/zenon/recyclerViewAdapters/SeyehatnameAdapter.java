package com.mobilcelebi.zenon.recyclerViewAdapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mobilcelebi.zenon.R;
import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.auth.Authentication;
import com.mobilcelebi.zenon.models.Comment;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.List;

public class SeyehatnameAdapter extends RecyclerView.Adapter<SeyehatnameAdapter.ViewHolder> {

    List<Comment> commentList;
    Context context;
    SharedPreferences sharedPreferences;
    Requests requests;

    public SeyehatnameAdapter(Context context, List<Comment> commentList) {
        this.context = context;
        this.commentList = commentList;
        sharedPreferences = context.getSharedPreferences("mobilcelebi", Context.MODE_PRIVATE);
        requests = new Requests();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.seyehatname_layout, parent, false);
        return new SeyehatnameAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d("TAG", "onBindViewHolder: " + commentList.get(position).getContent());
        holder.seyehatnameContent.setText(commentList.get(position).getContent());
        holder.seyehatnameLikeCount.setText(commentList.get(position).getLikeCount());
        holder.seyehatnameRating.setRating(commentList.get(position).getRating());
        try {
            String string = commentList.get(position).getPost().getString("title") + "'i " + (commentList.get(position).getRating() > 3 ? "beğendiniz" : "beğenmediniz");
            holder.seyahatnameYouLiked.setText(string);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            String id = new Authentication(sharedPreferences).getCurrentUser().getString("id");
            Picasso.with(context)
                    .load(Config.getServerIp() + "/api/users/" + id + "/profile_photo")
                    .into(holder.seyehatnameProfileImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView seyehatnameProfileImage;
        TextView seyehatnameContent, seyehatnameLikeCount,seyahatnameYouLiked;
        RatingBar seyehatnameRating;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            seyehatnameLikeCount = itemView.findViewById(R.id.seyehatnameLikeCount);
            seyehatnameContent = itemView.findViewById(R.id.seyehatnameDescription);
            seyehatnameProfileImage = itemView.findViewById(R.id.seyehatnameProfileImage);
            seyehatnameRating = itemView.findViewById(R.id.seyehatnameRating);
            seyahatnameYouLiked = itemView.findViewById(R.id.seyahatnameYouLiked);
        }
    }

}
