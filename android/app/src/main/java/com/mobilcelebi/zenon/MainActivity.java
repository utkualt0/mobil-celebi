package com.mobilcelebi.zenon;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.auth.Authentication;
import com.mobilcelebi.zenon.mainFragments.DiscoveryFragment;
import com.mobilcelebi.zenon.mainFragments.SearchFragment;
import com.mobilcelebi.zenon.mainFragments.ProfileFragment;
import com.ismaeldivita.chipnavigation.ChipNavigationBar;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;
    public static Context contextOfApplication;
    private final Fragment DEFAULT_FRAGMENT = Config.getDefaultFragment();
    private Authentication authentication;
    private SharedPreferences sharedPreferences;
    private boolean isUserLogged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        variables();
        setPolicy();
        listeners();
        loadFragment(DEFAULT_FRAGMENT);
        bottomMenuListeners();
    }

    private void listeners() {

    }

    private void variables() {
        bottomNavigationView = findViewById(R.id.bottom_nav_bar);
        contextOfApplication = getApplicationContext();
        sharedPreferences = getSharedPreferences("mobilcelebi",MODE_PRIVATE);
        authentication = new Authentication(sharedPreferences);
    }

    //Requestler için gerekli policy tanımlamaları
    private void setPolicy() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
    }

    private void loadFragment(Fragment fragment){getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,fragment).commit();}

    private void bottomMenuListeners(){
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;

                switch (item.getItemId()){
                    case R.id.bottomSearchNavBar:
                        fragment = new SearchFragment();
                        break;
                    case R.id.bottomDiscoveryNavBar:
                        fragment = new DiscoveryFragment();
                        break;
                    case R.id.bottomProfilNavBar:
                        fragment = new ProfileFragment();
                        break;
                    default:
                        fragment = new SearchFragment();
                        break;
                }
                loadFragment(fragment);
                return true;
            }
        });
    }
}
