package com.mobilcelebi.zenon.dal.auth;

import android.content.SharedPreferences;
import android.util.Log;

import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.Result;
import com.mobilcelebi.zenon.dal.auth.models.User;

import org.json.JSONException;
import org.json.JSONObject;

public class Authentication {

    private final SharedPreferences sharedPreferences;

    public Authentication(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public static Result login(String email, String password) {
        Requests requests = new Requests();

        return requests.createRequest(Config.getServerIp() + "/api/auth/login", Requests.POST)
                .addBodyItem("email", email)
                .addBodyItem("password", password)
                .build();
    }

    public static Result register(String username, String email, String password) {
        Requests requests = new Requests();

        return requests.createRequest(Config.getServerIp() + "/api/auth/register", Requests.POST)
                .addBodyItem("name", username)
                .addBodyItem("email", email)
                .addBodyItem("password", password)
                .build();
    }

    public String getAccessToken() {
        String access_token = sharedPreferences.getString("access_token", "");
        if (access_token.isEmpty()) {
            setUserIsNull();
        }
        return access_token;
    }

    public void setUserIsNull() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("currentUserInformation", "");
        editor.putString("access_token", "");
        editor.apply();
    }

    public void setCurrentUser(JSONObject userInformation) {
        if (userInformation.toString().isEmpty()) {
            setUserIsNull();
        } else {
            try {
                User user = new User(userInformation.getJSONObject("user"));
                user.saveAsCurrentUser(sharedPreferences);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("access_token",userInformation.getString("access_token"));
                editor.apply();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public JSONObject getCurrentUser() {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(sharedPreferences.getString("currentUserInformation", ""));
        } catch (JSONException e) {
            e.printStackTrace();
            jsonObject = new JSONObject();
        }
        return jsonObject;
    }

    public Result reLogin() {
        //TODO login ve registere işlemden sonra bu özellikleri prefe eklemesini ekle
        String email = sharedPreferences.getString("lastUserMail", "");
        String password = sharedPreferences.getString("lastUserPassword", "");
        return login(email, password);
    }
}
