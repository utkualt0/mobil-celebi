package com.mobilcelebi.zenon.recyclerViewAdapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.mobilcelebi.zenon.LoginActivity;
import com.mobilcelebi.zenon.PostDetailsActivity;
import com.mobilcelebi.zenon.R;
import com.mobilcelebi.zenon.RegisterActivity;
import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.Result;
import com.mobilcelebi.zenon.dal.auth.Authentication;
import com.mobilcelebi.zenon.helpers.CharTranslater;
import com.mobilcelebi.zenon.helpers.Slug;
import com.mobilcelebi.zenon.models.SearchResult;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.SearchViewHolder> {

    private Context context;
    private ArrayList<SearchResult> itemList;
    private Requests requests = new Requests();
    private SharedPreferences sharedPreferences;
    private Authentication authentication;

    class SearchViewHolder extends RecyclerView.ViewHolder {

        TextView cityCityName;
        TextView postPostName, postLikeCount, postCommentCount;
        ConstraintLayout cityLayout;
        ConstraintLayout postLayout;
        ImageView postShareLayout;
        ImageView socialItemRow;
        ImageView postLikeButton;

        public SearchViewHolder(@NonNull View itemView) {
            super(itemView);
            cityCityName = itemView.findViewById(R.id.allCategoriesCityName);
            postLayout = itemView.findViewById(R.id.postLayout);
            cityLayout = itemView.findViewById(R.id.cityLayout);
            postPostName = itemView.findViewById(R.id.postPostName);
            postLikeCount = itemView.findViewById(R.id.postLikeCount);
            postCommentCount = itemView.findViewById(R.id.postCommentCount);
            postShareLayout = itemView.findViewById(R.id.postShareButton);
            postLikeButton = itemView.findViewById(R.id.postLikeButton);
            socialItemRow = itemView.findViewById(R.id.socialItemRow);
        }
    }

    public SearchResultAdapter(Context context, ArrayList<SearchResult> itemList) {
        this.context = context;
        this.itemList = itemList;
        this.sharedPreferences = context.getSharedPreferences("mobilcelebi", Context.MODE_PRIVATE);
        authentication = new Authentication(sharedPreferences);
    }

    @NonNull
    @Override
    public SearchResultAdapter.SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_result_layout, parent, false);
        return new SearchResultAdapter.SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
        if (itemList.get(position).getType().equals("City")) {
            JSONObject data = itemList.get(position).getData();
            try {
                holder.cityCityName.setText(data.getString("name"));
                holder.cityLayout.setVisibility(View.VISIBLE);

                holder.cityLayout.setOnClickListener(v -> {
                    Intent i = new Intent(context, PostDetailsActivity.class);
                    i.putExtra("keyword", holder.cityCityName.getText().toString());
                    i.putExtra("keywordType", "City");
                    context.startActivity(i);
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (itemList.get(position).getType().equals("Post")) {
            AtomicReference<JSONObject> data = new AtomicReference<>(itemList.get(position).getData());
            try {
                holder.postPostName.setText(data.get().getString("title"));
                holder.postLikeCount.setText(data.get().get("likeCount").toString());
                holder.postCommentCount.setText(data.get().get("commentCount").toString());
                holder.postLayout.setVisibility(View.VISIBLE);

                holder.postLikeButton.setOnClickListener(v -> {
                        try {
                            if(new Authentication(sharedPreferences).getCurrentUser().isNull("id")){
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Hesap Aç")
                                        .setMessage("Bu işlemi yapabilmek için hesabına giriş yapmış olman gerekiyor")
                                        .setNeutralButton("Kapat", (dialog, which) -> dialog.dismiss())
                                        .setNegativeButton("Giriş Yap", (dialog, which) -> {
                                            context.startActivity(new Intent(context, LoginActivity.class));
                                            dialog.dismiss();
                                        })
                                        .setPositiveButton("Kayıt Ol", (dialog, which) -> {
                                            context.startActivity(new Intent(context, RegisterActivity.class));
                                            dialog.dismiss();
                                        })
                                        .show();
                                return;
                            }
                            requests.createRequest(
                                    data.get().getJSONArray("likes").toString().contains(new Authentication(sharedPreferences).getCurrentUser().getString("id")) ?
                                            Config.getServerIp() + "/api/posts/" + data.get().getString("_id") + "/undo_like" :
                                            Config.getServerIp() + "/api/posts/" + data.get().getString("_id") + "/like",
                                    Requests.GET)
                                    .addHeader(
                                            "Authorization",
                                            "Bearer: " + sharedPreferences.getString("access_token", ""))
                                    .build()
                                    .onSuccess(result -> {
                                        if (result.isSuccess()) {
                                            try {
                                                holder.postLikeCount.setText(result.getJson().getJSONObject("data").getString("likeCount"));
                                                data.set(result.getJson().getJSONObject("data"));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        else if(result.getStatus() == 401){
                                            Result reloginRes = authentication.reLogin();
                                            if(!reloginRes.isSuccess()){
                                                Toast.makeText(context, "Bir sorun oluştu lütfen daha sonra tekrar dene", Toast.LENGTH_SHORT).show();
                                                authentication.setUserIsNull();
                                            }
                                            else{
                                                Toast.makeText(context, "Bir sorun oluştu lütfen tekrar dene", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                        else {
                                            Toast.makeText(context, "Bir sorun oluştu!", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                });

                holder.postLayout.setOnClickListener(v -> {
                    Toast.makeText(context, "Seçilen gönderi: " + holder.postPostName.getText().toString(), Toast.LENGTH_SHORT).show();
                });

                holder.postShareLayout.setOnClickListener(v -> {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);

                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, "https://mobilcelebi.com" + "/p/" + Slug.toSlug(CharTranslater.translateChars(holder.postPostName.getText().toString())));
                    holder.itemView.getContext().startActivity(Intent.createChooser(intent, "Share"));
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}
