package com.mobilcelebi.zenon.mainFragments.profileFragment.settingsFragment;

import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.mobilcelebi.zenon.MainActivity;
import com.mobilcelebi.zenon.R;
import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.Result;
import com.mobilcelebi.zenon.dal.auth.Authentication;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class ProfileSettingsFragment extends Fragment {

    private static final Requests requests = new Requests();
    private View view;
    private Context context;
    private SharedPreferences sharedPreferences;
    private ImageView profilePhotoEdit, goBackToProfile;
    private EditText profileNameEdit, profileMailEdit;
    private Button editProfileButton;
    private TextView changeProfilePhotoText;
    private Authentication authentication;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View createView = inflater.inflate(R.layout.fragment_profile_settings, container, false);
        this.view = createView;
        init();
        return createView;
    }

    private void init() {
        variables();
        listeners();
        Result result = getInfo();
        if (result.isSuccess()) {
            loadInfo(result);
        } else if (result.getStatus() == 401) {
            Result reLoginResult = authentication.reLogin();

            if (!reLoginResult.isSuccess()) {
                authentication.setUserIsNull();
                Toast.makeText(context, "Bir sorun oluştu!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Bir sorun oluştu lütfen tekrar dene!", Toast.LENGTH_SHORT).show();
            }
            context.startActivity(new Intent(context, MainActivity.class));
            getActivity().finish();
        } else {
            context.startActivity(new Intent(context, MainActivity.class));
            getActivity().finish();
        }
    }

    private void variables() {
        sharedPreferences = context.getSharedPreferences("mobilcelebi", Context.MODE_PRIVATE);
        profilePhotoEdit = view.findViewById(R.id.profilePhoto);
        profileMailEdit = view.findViewById(R.id.profileMailEdit);
        profileNameEdit = view.findViewById(R.id.profileNameEdit);
        editProfileButton = view.findViewById(R.id.editProfileButton);
        changeProfilePhotoText = view.findViewById(R.id.changeProfilePhotoText);
        goBackToProfile = view.findViewById(R.id.goBackToProfile);
        authentication = new Authentication(sharedPreferences);
    }

    private void listeners() {
        profilePhotoEdit.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("https://mobilcelebi.com/profile/"));
            startActivity(intent);
        });
        editProfileButton.setOnClickListener(v -> sendInfo());
        changeProfilePhotoText.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("https://mobilcelebi.com/profile/"));
            startActivity(intent);
        });
        goBackToProfile.setOnClickListener(v -> getFragmentManager().popBackStack());
    }

    private void sendInfo() {
        requests.createRequest(Config.getServerIp() + "/api/auth/edit", Requests.UPDATE)
                .addBodyItem("name", profileNameEdit.getText().toString())
                .addBodyItem("email", profileMailEdit.getText().toString())
                .addHeader("Authorization", "Bearer: " + sharedPreferences.getString("access_token", ""))
                .build()
                .onSuccess(result -> {
                    if (result.isSuccess()) {
                        startActivity(new Intent(context, MainActivity.class));
                        getActivity().finish();
                        Toast.makeText(context, "Kaydedildi!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Bir hata oluştu!", Toast.LENGTH_SHORT).show();
                        getFragmentManager().popBackStack();
                    }
                });
    }

    private void loadInfo(Result result) {
        try {
            JSONObject user = result.getJson().getJSONObject("user");
            profileNameEdit.setText(user.getString("name"));
            profileMailEdit.setText(user.getString("email"));
            getProfilePhoto();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Result getInfo() {
        return requests.createRequest(Config.getServerIp() + "/api/users/getMyProfile", Requests.GET)
                .addHeader("Authorization", "Bearer: " + sharedPreferences.getString("access_token", ""))
                .build();
    }

    private void getProfilePhoto() {
        try {
            String id = new Authentication(sharedPreferences).getCurrentUser().getString("id");
            Picasso.with(context)
                    .load(Config.getServerIp() + "/api/users/" + id + "/profile_photo")
                    .into(profilePhotoEdit);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}