package com.mobilcelebi.zenon.onboarding;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.mobilcelebi.zenon.LoginActivity;
import com.mobilcelebi.zenon.MainActivity;
import com.mobilcelebi.zenon.R;
import com.mobilcelebi.zenon.RegisterActivity;

public class OnBoarding5 extends Fragment {
    private TextView atlaB5;
    private Button kayıtOlFragment,girisYapFragment;
    private Activity splashScreen;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private ProgressBar onBoardingProgressBar5;

    public OnBoarding5(Activity splashScreen) {
        this.splashScreen = splashScreen;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_onboarding_5, container,false);
        atlaB5 = root.findViewById(R.id.skipButtonP5);
        kayıtOlFragment = root.findViewById(R.id.kayitOlFragment);
        girisYapFragment = root.findViewById(R.id.girisYapFragment);
        onBoardingProgressBar5 = root.findViewById(R.id.onBoardingProgressBar5);

        sharedPreferences = splashScreen.getSharedPreferences("mobilcelebi",Context.MODE_PRIVATE);
        editor= sharedPreferences.edit();

        atlaB5.setOnClickListener(v->{
            startActivity(new Intent(splashScreen, MainActivity.class));
            onBoardingProgressBar5.setVisibility(View.VISIBLE);
            splashScreen.finish();
            editor.putBoolean("firstLogin",false);
            editor.commit();
        });
        kayıtOlFragment.setOnClickListener(v->{
            startActivity(new Intent(splashScreen, RegisterActivity.class));
            splashScreen.finish();
            editor.putBoolean("firstLogin",false);
            editor.commit();
        });
        girisYapFragment.setOnClickListener(v->{
            startActivity(new Intent(splashScreen, LoginActivity.class));
            splashScreen.finish();
            editor.putBoolean("firstLogin",false);
            editor.commit();
        });

        return root;
    }
}
