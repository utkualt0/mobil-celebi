package com.mobilcelebi.zenon.models;

public class Post {

    //Card Post Objesi

    private String başlık;
    private String içerik;
    private String id;

    public Post(String başlık, String içerik, String id) {
        this.başlık = başlık;
        this.içerik = içerik;
        this.id = id;
    }

    public String getBaşlık() {
        return başlık;
    }

    public void setBaşlık(String başlık) {
        this.başlık = başlık;
    }

    public String getIçerik() {
        return içerik;
    }

    public void setIçerik(String içerik) {
        this.içerik = içerik;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
