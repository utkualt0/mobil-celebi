package com.mobilcelebi.zenon.categoriesFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class FragmentAdapter extends FragmentStatePagerAdapter {


    public FragmentAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                return new AllFragment();

            case 1:
                return new HistoricalFragment();

            case 2:
                return new SocialFragment();

            default:
                return new FoodFragment();
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0:
                title = "Hepsi";
                break;
            case 1:
                title = "Tarihi Mekanlar";
                break;
            case 2:
                title = "Sosyal Mekanlar";
                break;
            case 3:
                title = "Yemekler";
                break;
        }
        return title;
    }
}
