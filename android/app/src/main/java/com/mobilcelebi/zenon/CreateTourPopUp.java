package com.mobilcelebi.zenon;

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.auth.Authentication;

import org.json.JSONException;

import java.util.Calendar;

public class CreateTourPopUp extends AppCompatActivity {

    private Button createTourButton, createTourSave;
    private FrameLayout touchInterceptor;
    private TextView startTourTextView, finishTourTextView, tourNameTextView, aciklamaTextView, tourCreateHostName;
    private EditText tourNameEditText, aciklamaEditText;
    private String startDate, finishDate;
    private final Requests requests = new Requests();
    private SharedPreferences sharedPreferences;
    private ImageView tourGoBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_tour);
        init();
    }

    private void init() {
        variables();
        listeners();
        setHostName();
        tourCalendarStart();
        tourCalendarFinish();
    }

    private void listeners() {
        createTourButton.setOnClickListener(v -> {
            if (startDate.isEmpty() || finishDate.isEmpty() || tourNameEditText.getText().toString().isEmpty() || aciklamaEditText.getText().toString().isEmpty()) {
                Toast.makeText(this, "Lütfen hiçbir yeri boş bırakmayınız", Toast.LENGTH_SHORT).show();
                return;
            }
            requests.createRequest(Config.getServerIp() + "/api/tour?ownMember=true", Requests.POST)
                    .addBodyItem("title", tourNameEditText.getText().toString())
                    .addBodyItem("startDate", startDate)
                    .addBodyItem("finishDate", finishDate)
                    .addBodyItem("description", aciklamaEditText.getText().toString())
                    .addHeader("Authorization", "Bearer: " + sharedPreferences.getString("access_token", ""))
                    .build()
                    .onSuccess(result -> {
                        if (result.isSuccess()) {
                            Toast.makeText(this, "Tur oluşturuldu!", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        Log.d("TAG", "listeners: " + result.getJson());
                    });
        });
        createTourButton.setOnClickListener(v -> finish());
        tourGoBack.setOnClickListener(v-> finish());
    }

    private void variables() {
        sharedPreferences = getSharedPreferences("mobilcelebi", MODE_PRIVATE);
        startTourTextView = findViewById(R.id.startTourTextView);
        tourCreateHostName = findViewById(R.id.tourCreateHostName);
        finishTourTextView = findViewById(R.id.finishTourTextView);
        createTourButton = findViewById(R.id.createTourButton);
        tourGoBack = findViewById(R.id.tourGoBack);
        tourNameEditText = findViewById(R.id.tourNameEditText);
        aciklamaEditText = findViewById(R.id.aciklamaEditText);
//     aciklamaTextView = findViewById(R.id.aciklamaTextView);
//        tourNameTextView = findViewById(R.id.tourNameTextView);
    }

    private void setHostName() {
        try {
            String name = new Authentication(sharedPreferences).getCurrentUser().getString("name");
            tourCreateHostName.setText(name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void tourCalendarStart() {
        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);

        startTourTextView.setOnClickListener(v -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                    month = month + 1;
                    String date = day + "-" + month + "-" + year;
                    startTourTextView.setText(date);
                    startDate = year + "-" + month + "-" + day;
                }
            }, year, month, day);

            datePickerDialog.show();
        });
    }

    private void tourCalendarFinish() {
        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);

        finishTourTextView.setOnClickListener(v -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                    month = month + 1;
                    String date = day + "-" + month + "-" + year;
                    finishTourTextView.setText(date);
                    finishDate = year + "-" + month + "-" + day;
                }
            }, year, month, day);

            datePickerDialog.show();
        });
    }
}