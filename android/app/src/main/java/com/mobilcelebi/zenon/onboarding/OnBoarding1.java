package com.mobilcelebi.zenon.onboarding;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.mobilcelebi.zenon.MainActivity;
import com.mobilcelebi.zenon.R;

public class OnBoarding1 extends Fragment {
    private TextView atlaB;
    private Activity splashScreen;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private ProgressBar onBoardingProgressBar1;

    public OnBoarding1(Activity splashScreen) {
        this.splashScreen = splashScreen;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_onboarding_1, container,false);
        atlaB = root.findViewById(R.id.skipButtonP1);
        onBoardingProgressBar1 = root.findViewById(R.id.onBoardingProgressBar1);

        sharedPreferences = splashScreen.getSharedPreferences("mobilcelebi",Context.MODE_PRIVATE);
        editor= sharedPreferences.edit();

        atlaB.setOnClickListener(v->{
            onBoardingProgressBar1.setVisibility(View.VISIBLE);
            startActivity(new Intent(splashScreen, MainActivity.class));
            splashScreen.finish();
            editor.putBoolean("firstLogin",false);
            editor.commit();
        });
        return root;
    }
}


