package com.mobilcelebi.zenon.dal;

import android.util.Log;

import com.mobilcelebi.zenon.dal.listeners.onCompleteListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class Result {

    private String message;
    private boolean success;
    private final String stringData;
    private IOException error;
    private JSONObject json;
    private int status;

    public Result(String message, String responseBody,int status) {
        this.message = message;
        this.stringData = responseBody;
        this.status = status;
        this.success = status == 200;
        try{
            this.json = new JSONObject(responseBody);
        }catch (Exception e){
            this.message = responseBody;
            this.json = new JSONObject();
        }
    }

    public Result(IOException error){
        this.error = error;
        this.success = false;
        this.message = "Request failed!";
        this.stringData = error.toString();
    }

    public Result onSuccess(onCompleteListener onCompleteListener){
        onCompleteListener.completeListener(this);
        return this;
    }

    public JSONObject getJson() {
        return json;
    }

    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getStringData() {
        return stringData;
    }

    public IOException getError() {
        return error;
    }

    public int getStatus() {
        return status;
    }
}
