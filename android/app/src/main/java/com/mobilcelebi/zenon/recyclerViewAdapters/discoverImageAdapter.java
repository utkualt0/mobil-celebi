package com.mobilcelebi.zenon.recyclerViewAdapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mobilcelebi.zenon.PostDetailsActivity;
import com.mobilcelebi.zenon.R;
import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.Result;
import com.mobilcelebi.zenon.dal.auth.Authentication;
import com.mobilcelebi.zenon.helpers.CharTranslater;
import com.mobilcelebi.zenon.helpers.Slug;
import com.mobilcelebi.zenon.models.DiscoverImage;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.List;

public class discoverImageAdapter extends RecyclerView.Adapter<discoverImageAdapter.ImageViewHolder> {

    private final Context context;
    private final List<DiscoverImage> imageItems;
    private final Requests requests = new Requests();
    private SharedPreferences sharedPreferences;
    private Authentication authentication;

    public discoverImageAdapter(Context context, List<DiscoverImage> imageItems) {
        this.context = context;
        this.imageItems = imageItems;
        sharedPreferences = context.getSharedPreferences("mobilcelebi",Context.MODE_PRIVATE);
        authentication = new Authentication(sharedPreferences);
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ImageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.discover_image_container, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        holder.setImageData(imageItems.get(position));
    }

    @Override
    public int getItemCount() {
        return imageItems.size();
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView, discoverLikeButton;
        TextView textImageTitle, textImageDescription,discoverLikeCount,discoverCommentCount;
        ProgressBar imageProgressBar;

        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.discoverImageView);
            textImageTitle = itemView.findViewById(R.id.discoverImageTitle);
            textImageDescription = itemView.findViewById(R.id.discoverImageDescription);
            imageProgressBar = itemView.findViewById(R.id.discoverImageProgressBar);
            discoverLikeButton = itemView.findViewById(R.id.discoverLikeButton);
            discoverCommentCount = itemView.findViewById(R.id.discoverCommentCount);
            discoverLikeCount = itemView.findViewById(R.id.discoverLikeCount);
        }

        void setImageData(DiscoverImage image) {
            textImageTitle.setText(image.imageTitle);
            int MAX_LENGTH = 120;
            textImageDescription.setText(image.imageDescription.length() >= MAX_LENGTH ? image.imageDescription.substring(0, MAX_LENGTH - 3) + "..." : image.imageDescription);
            discoverLikeCount.setText(image.likeCount);
            discoverCommentCount.setText(image.commentCount);
            Picasso.with(context)
                    .load(image.imageURL)
                    .into(imageView);
            if(image.like){
                discoverLikeButton.setImageResource(R.drawable.like_icon_fill);
                image.like = true;
            }
            textImageDescription.setOnClickListener(v -> {
                if (textImageDescription.getText().length() <= MAX_LENGTH) {
                    textImageDescription.setText(image.imageDescription);
                } else if (image.imageDescription.length() > MAX_LENGTH) {
                    textImageDescription.setText(image.imageDescription.substring(0, MAX_LENGTH - 3) + "...");
                }
            });
            imageView.setOnClickListener(v -> {
                Intent intent = new Intent(context, PostDetailsActivity.class);
                intent.putExtra("keywordType", "Post");
                intent.putExtra("keyword", Slug.toSlug(CharTranslater.translateChars(textImageTitle.getText().toString())));
                context.startActivity(intent);
            });
            discoverLikeButton.setOnClickListener(v -> {
                likeDiscover(image);
            });
        }

        void likeDiscover(DiscoverImage image) {
            requests.createRequest(
                    image.like ?
                            Config.getServerIp() + "/api/posts/" + image.postID + "/undo_like" :
                            Config.getServerIp() + "/api/posts/" + image.postID + "/like",
                    Requests.GET)
                    .addHeader(
                            "Authorization",
                            "Bearer: " + sharedPreferences.getString("access_token", ""))
                    .build()
                    .onSuccess(result -> {
                        if (result.isSuccess()) {
                            try {
                                discoverLikeCount.setText(result.getJson().getJSONObject("data").get("likeCount").toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else if(result.getStatus() == 401){
                            Result reloginRes = authentication.reLogin();
                            if(!reloginRes.isSuccess()){
                                Toast.makeText(context, "Bir sorun oluştu lütfen daha sonra tekrar dene", Toast.LENGTH_SHORT).show();
                                authentication.setUserIsNull();
                            }
                            else{
                                Toast.makeText(context, "Bir sorun oluştu lütfen tekrar dene", Toast.LENGTH_SHORT).show();
                            }
                            discoverLikeButton.setImageResource(R.drawable.like_icon_border);
                            image.like = false;
                        }
                        else{
                            discoverLikeButton.setImageResource(R.drawable.like_icon_border);
                            image.like = false;
                            Toast.makeText(context, "Bir sorun oluştu!", Toast.LENGTH_SHORT).show();
                        }
                    });

            if (image.like) {
                discoverLikeButton.setImageResource(R.drawable.like_icon_border);
                image.like = false;
            } else {
                discoverLikeButton.setImageResource(R.drawable.like_icon_fill);
                image.like = true;
            }
        }
    }
}
