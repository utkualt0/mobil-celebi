package com.mobilcelebi.zenon;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mobilcelebi.zenon.config.Config;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RowItemAdapterMainTopFives extends RecyclerView.Adapter<RowItemAdapterMainTopFives.ViewHolder> {

    ArrayList<RowImageTopFive> rowImageRecyclerViews;
    Context context;

    public RowItemAdapterMainTopFives(Context context, ArrayList<RowImageTopFive> rowImageRecyclerViews) {
        this.context = context;
        this.rowImageRecyclerViews = rowImageRecyclerViews;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_all_categories, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.postCityName.setText(rowImageRecyclerViews.get(position).getCityName());
        holder.postDescripton.setText(rowImageRecyclerViews.get(position).getCityDescription());
        holder.itemView.setOnClickListener(v -> {
            Intent i = new Intent(context,PostDetailsActivity.class);
            i.putExtra("keyword",holder.postCityName.getText().toString());
            i.putExtra("keywordType","Post");
            context.startActivity(i);
        });

        Picasso.with(context)
                .load(rowImageRecyclerViews.get(position).getUrl())
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return rowImageRecyclerViews.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView postCityName;
        TextView postDescripton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.rowItemImage);
            postCityName = itemView.findViewById(R.id.allCategoriesPostName);
            postDescripton = itemView.findViewById(R.id.categoriesResultDescription);

        }
    }
}
