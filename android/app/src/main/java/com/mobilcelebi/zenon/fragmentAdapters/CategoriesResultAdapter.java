package com.mobilcelebi.zenon.fragmentAdapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.mobilcelebi.zenon.LoginActivity;
import com.mobilcelebi.zenon.PostDetailsActivity;
import com.mobilcelebi.zenon.R;
import com.mobilcelebi.zenon.RegisterActivity;
import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.auth.Authentication;
import com.mobilcelebi.zenon.helpers.CharTranslater;
import com.mobilcelebi.zenon.helpers.Slug;
import com.mobilcelebi.zenon.models.SearchResult;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

public class CategoriesResultAdapter extends RecyclerView.Adapter<CategoriesResultAdapter.SearchViewHolder> {

    Context context;
    ArrayList<SearchResult> itemList;
    Requests requests = new Requests();
    SharedPreferences sharedPreferences;

    public CategoriesResultAdapter(Context context, ArrayList<SearchResult> itemList) {
        this.context = context;
        this.itemList = itemList;
        this.sharedPreferences = context.getSharedPreferences("mobilcelebi", Context.MODE_PRIVATE);
    }

    @NonNull
    @Override
    public CategoriesResultAdapter.SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_categories_result_adapter, parent, false);
        return new CategoriesResultAdapter.SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
        Log.d("TAG", "onBindViewHolder: ");
        if (itemList.get(position).getType() == SearchResult.Types.Post) {
            AtomicReference<JSONObject> data = new AtomicReference<>(itemList.get(position).getData());
            try {
                int MAX_LENGTH = 235;
                holder.categoriesResultDescription.setText(data.get().getString("description").length() >= MAX_LENGTH ? data.get().getString("description").substring(0, MAX_LENGTH - 3) + "..." : data.get().getString("description"));
                holder.postLikeCount.setText(data.get().get("likeCount").toString());
                holder.postCommentCount.setText(data.get().get("commentCount").toString());
                holder.postPostName.setText(data.get().get("title").toString());
                if (data.get().getJSONArray("images").length() > 0)
                    Picasso.with(context)
                            .load(data.get().getJSONArray("images").getString(0))
                            .into(holder.categoriesPhoto);

                holder.postLikeButton.setOnClickListener(v -> {
                    try {
                        if (new Authentication(sharedPreferences).getCurrentUser().isNull("id")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle("Hesap Aç")
                                    .setMessage("Bu işlemi yapabilmek için hesabına giriş yapmış olman gerekiyor")
                                    .setNeutralButton("Kapat", (dialog, which) -> dialog.dismiss())
                                    .setNegativeButton("Giriş Yap", (dialog, which) -> {
                                        context.startActivity(new Intent(context, LoginActivity.class));
                                        dialog.dismiss();
                                    })
                                    .setPositiveButton("Kayıt Ol", (dialog, which) -> {
                                        context.startActivity(new Intent(context, RegisterActivity.class));
                                        dialog.dismiss();
                                    })
                                    .show();
                            return;
                        }
                        requests.createRequest(
                                data.get().getJSONArray("likes").toString().contains(new Authentication(sharedPreferences).getCurrentUser().getString("id")) ?
                                        Config.getServerIp() + "/api/posts/" + data.get().getString("_id") + "/undo_like" :
                                        Config.getServerIp() + "/api/posts/" + data.get().getString("_id") + "/like",
                                Requests.GET)
                                .addHeader(
                                        "Authorization",
                                        "Bearer: " + sharedPreferences.getString("access_token", ""))
                                .build()
                                .onSuccess(result -> {
                                    if (result.isSuccess()) {
                                        try {
                                            holder.postLikeCount.setText(result.getJson().getJSONObject("data").getString("likeCount"));
                                            data.set(result.getJson().getJSONObject("data"));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        Toast.makeText(context, "Bir sorun oluştu!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                holder.postLayout.setOnClickListener(v -> {
                    Intent i = new Intent(context, PostDetailsActivity.class);
                    i.putExtra("keyword", holder.postPostName.getText().toString());
                    i.putExtra("keywordType", "Post");
                    context.startActivity(i);
                });

                holder.postShareLayout.setOnClickListener(v -> {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);

                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, "https://mobilcelebi.com" + "/p/" + Slug.toSlug(CharTranslater.translateChars(holder.postPostName.getText().toString())));
                    holder.itemView.getContext().startActivity(Intent.createChooser(intent, "Share"));
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class SearchViewHolder extends RecyclerView.ViewHolder {

        TextView cityCityName, categoriesResultDescription;
        TextView postPostName, postLikeCount, postCommentCount;
        ConstraintLayout cityLayout;
        ConstraintLayout postLayout;
        ImageView postShareLayout;
        ImageView socialItemRow;
        ImageView postLikeButton;
        ImageView categoriesPhoto;

        public SearchViewHolder(@NonNull View itemView) {
            super(itemView);
            categoriesResultDescription = itemView.findViewById(R.id.categoriesResultDescription);
            postLayout = itemView.findViewById(R.id.postLayout);
            cityLayout = itemView.findViewById(R.id.cityLayout);
            postPostName = itemView.findViewById(R.id.categoriesCityName);
            postLikeCount = itemView.findViewById(R.id.likeCount);
            postCommentCount = itemView.findViewById(R.id.categoriesCommentCount);
            postShareLayout = itemView.findViewById(R.id.categoriesShareIcon);
            postLikeButton = itemView.findViewById(R.id.likeIcon);
            socialItemRow = itemView.findViewById(R.id.socialItemRow);
            categoriesPhoto = itemView.findViewById(R.id.categoriesPhoto);
        }
    }
}
