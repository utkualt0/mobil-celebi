package com.mobilcelebi.zenon.mainFragments.profileFragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobilcelebi.zenon.R;

public class SettingsBottomSheet extends Fragment {

    private View profileSettingsLayout;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View createView = inflater.inflate(R.layout.fragment_settings_bottom_sheet, container, false);
        this.view = createView;
        return createView;
    }

}
