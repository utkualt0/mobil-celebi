package com.mobilcelebi.zenon.recyclerViewAdapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mobilcelebi.zenon.PostDetailsActivity;
import com.mobilcelebi.zenon.R;
import com.mobilcelebi.zenon.SearchResultActivity;
import com.mobilcelebi.zenon.helpers.CharTranslater;
import com.mobilcelebi.zenon.helpers.Slug;
import com.mobilcelebi.zenon.models.SearchResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {
    Context context;
    ArrayList<SearchResult> itemList;

    public SearchAdapter(Context context, ArrayList<SearchResult> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public SearchAdapter.SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_list_item_layout, parent, false);
        return new SearchAdapter.SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
        SearchResult object = itemList.get(position);
        setText(object, position, holder);
        if(position != 0)
            setListener(object, position, holder);
    }

    void setText(SearchResult object, int position, SearchViewHolder holder) {
        try {
            JSONObject data = object.getData();
            String title;
            if (object.getType() == SearchResult.Types.Post){
                title = data.getString("title");
            }
            else{
                title = data.getString("name");
            }
            String text = title.toLowerCase().trim();;
            String keyword;
            try {
                keyword = data.getString("searchKeyword");
            } catch (Exception e) {
                keyword = Slug.toSlug(CharTranslater.translateChars(text));
            }
            if (!text.contains(keyword)) {
                holder.full_name.setText(title);
                return;
            }
            if (position != 0) {
                int start = text.indexOf(data.getString("searchKeyword"));
                int end = start + data.getString("searchKeyword").length();
                SpannableString ss = new SpannableString(title);
                StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
                ss.setSpan(boldSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.full_name.setText(ss);
            } else {
                holder.full_name.setText(text);
                holder.full_name.setTypeface(null, Typeface.BOLD);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void setListener(SearchResult object, int position, SearchViewHolder holder) {
        String searchKeyword = null;
        try {
            searchKeyword = object.getType() == SearchResult.Types.Post ? object.getData().getString("title") : object.getData().getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (object.getType() == SearchResult.Types.Post) {
            Intent i = new Intent(context, PostDetailsActivity.class);
            i.putExtra("keyword", searchKeyword);
            i.putExtra("keywordType", "Post");
            holder.searchLayoutLayout.setOnClickListener(v -> context.startActivity(i));
        } else if (object.getType() == SearchResult.Types.City) {
            Intent intent = new Intent(context, SearchResultActivity.class);
            intent.putExtra("searchKeyword", searchKeyword);
            holder.full_name.setOnClickListener(v -> context.startActivity(intent));
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    class SearchViewHolder extends RecyclerView.ViewHolder {

        TextView full_name;
        LinearLayout searchLayoutLayout;

        public SearchViewHolder(@NonNull View itemView) {
            super(itemView);
            full_name = (TextView) itemView.findViewById(R.id.full_name);
            searchLayoutLayout = itemView.findViewById(R.id.searchLayoutLayout);
        }
    }


//        try{
//            if (itemList.get(position).getType() == SearchResult.Types.Post){
//                String text = itemList.get(position).getData().getString("title").toLowerCase();
//                String title = itemList.get(position).getData().getString("title");
//                String keyword;
//                try{
//                     keyword= itemList.get(position).getData().getString("searchKeyword");
//                }
//                catch (Exception e){
//                    keyword = Slug.toSlug(CharTranslater.translateChars(title.trim().toLowerCase()));
//                }
//                if (text.indexOf(keyword)==-1){
//                    holder.full_name.setText(title);
//                }
//                if (position!=0){
//                    int start = text.indexOf(itemList.get(position).getData().getString("searchKeyword"));
//                    int end = itemList.get(position).getData().getString("searchKeyword").length()+start;
//                    SpannableString ss = new SpannableString(title);
//                    StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
//                    ss.setSpan(boldSpan, start,end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//                    holder.full_name.setText(ss);
//                }
//                else{
//                    holder.full_name.setText(text);
//                    holder.full_name.setTypeface(null,Typeface.BOLD);
//                }
//                holder.searchLayoutLayout.setOnClickListener(v ->{
//                    String searchKeyword = null;
//                    try {
//                        searchKeyword = itemList.get(position).getData().getString("title");
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    if (TextUtils.isEmpty(searchKeyword)) {
//                        Toast.makeText(context, "Lütfen bir kelime giriniz", Toast.LENGTH_SHORT).show();
//                    }
//                    Intent i = new Intent(context, PostDetailsActivity.class);
//                    i.putExtra("keyword", searchKeyword);
//                    i.putExtra("keywordType", "Post");
//                    context.startActivity(i);
//                });
//            }
//            if (itemList.get(position).getType() == SearchResult.Types.City){
//                holder.full_name.setText(itemList.get(position).getData().getString("name"));
//                holder.searchLayoutLayout.setOnClickListener(v ->{
//                    String searchKeyword = null;
//                    try {
//                        searchKeyword = itemList.get(position).getData().getString("title");
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    if (TextUtils.isEmpty(searchKeyword)) {
//                        Toast.makeText(context, "Lütfen bir kelime giriniz", Toast.LENGTH_SHORT).show();
//                    } else {
//                        context.startActivity(new Intent(context, SearchResultActivity.class).putExtra("searchKeyword", searchKeyword));
//                    }
//                });
//            }
//        }
//        catch (JSONException e) {
//            e.printStackTrace();
//         }
}
