package com.mobilcelebi.zenon;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mobilcelebi.zenon.config.Config;
import com.mobilcelebi.zenon.dal.Requests;
import com.mobilcelebi.zenon.dal.Result;
import com.mobilcelebi.zenon.dal.auth.Authentication;

public class FeedbackActivity extends AppCompatActivity {

    Button feedbackSubmit;
    Spinner spinner;
    EditText feedbackDescription, feedbackTitle;
    Requests requests = new Requests();
    SharedPreferences sharedPreferences;
    Context context;
    Authentication authentication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        init();
    }

    private void init() {
        variables();
        listeners();
    }

    private void variables() {
        feedbackSubmit = findViewById(R.id.feedbackSubmit);
        spinner = findViewById(R.id.spinner);
        feedbackDescription = findViewById(R.id.feedbackDescription);
        feedbackTitle = findViewById(R.id.feedbackTitle);
        sharedPreferences = getSharedPreferences("mobilcelebi",MODE_PRIVATE);
        context = this;
        authentication = new Authentication(sharedPreferences);
    }

    private void listeners() {
        feedbackTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 20){
                    feedbackTitle.setText(feedbackTitle.getText().toString().substring(0,20));
                    Toast.makeText(context, "Başlık en fazla 20 karakter olabilir", Toast.LENGTH_SHORT).show();
                }
            }
        });

        feedbackSubmit.setOnClickListener(v -> {
            String title = feedbackTitle.getText().toString();
            String description = feedbackDescription.getText().toString();
            String type = spinner.getSelectedItem().toString();
            if(title.isEmpty() || description.isEmpty()){
                Toast.makeText(this, "Lütfen boş yer bırakmayınız", Toast.LENGTH_SHORT).show();
                return;
            }
            requests.createRequest(Config.getServerIp() + "/api/feedback",Requests.POST)
                    .addHeader("Authorization","Bearer: " + sharedPreferences.getString("access_token",""))
                    .addBodyItem("title",title)
                    .addBodyItem("content",description)
                    .addBodyItem("type",type)
                    .build()
            .onSuccess(result -> {
                Log.d("TAG", "listeners: " + result.getStringData());
                if(result.isSuccess()){
                    Toast.makeText(this, "Geri bildirim iletildi. Bizimle iletişime geçtiğin için teşekkür ederiz.", Toast.LENGTH_SHORT).show();
                    finish();
                }
                else if(result.getStatus() == 401){
                    Result reLoginResult = authentication.reLogin();
                    if (!reLoginResult.isSuccess()) {
                        authentication.setUserIsNull();
                        Toast.makeText(context, "Bir sorun oluştu!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(context, "Bir sorun oluştu lütfen daha sonra dene!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
                else{
                    Toast.makeText(this, "Bir sorun oluştu.", Toast.LENGTH_SHORT).show();
                }
            });
        });
    }

}
